#include "stdafx.h"

#include "CrossVal.h"

#include <opencv2/imgproc.hpp>

#include "PConfig.h"

#define SAMPLESIZE 5

using namespace std; 
using namespace cv;

CCrossValidation::CCrossValidation()
{
    m_offset = 0;
}

CCrossValidation::~CCrossValidation()
{

}

void CCrossValidation::FillVectors()
{
    if (m_vec_categories.size() == 0)
    {
        Load();
    }
}

void CCrossValidation::SetFolds(int folds)
{
    m_Folds = folds;
}

//Metodo que carrega todas as imagens
void CCrossValidation::Load()
{
    vector<String> auxLista;
    
    m_vec_categories.clear();

    String auxConv = GetPathToDump1();
    glob(auxConv, auxLista, false);
    m_vec_categories.emplace_back(auxLista);
    
    auxLista.clear();

    auxConv = GetPathToDump2();
    glob(auxConv, auxLista, false);
    m_vec_categories.emplace_back(auxLista);
}

void CCrossValidation::KFold(int fold)
{
    m_vec_ConfMatrix.clear();
    m_vec_validation.clear();
    m_vec_trainingdt.clear();

    if (!m_console)
    {
        ASSERT("Console is NULL");
        return;
    }

    vector<String> aux0 = m_vec_categories[0]; //categoria 1
    vector<String> aux1 = m_vec_categories[1]; //categoria 2

    int foldSize  = (aux0.size() + aux1.size()) / m_Folds;
    int foldSizeC = foldSize / m_vec_categories.size();

    int offset0 = fold*foldSizeC;
    int offset1 = fold*foldSizeC;

    vector<String> feedAux;
    for (int j = 0; j < aux0.size(); j++)
    {
        if (j < offset0)
        {
            feedAux.emplace_back(aux0[j]);
        }
        else if (j > offset0 + foldSizeC)
        {
            feedAux.emplace_back(aux0[j]);
        }
        else
        {
            m_vec_validation.emplace_back(aux0[j]);
            m_vec_ConfMatrix.emplace_back(1);
        }
    }
    m_vec_trainingdt.emplace_back(feedAux);

    feedAux.clear();
    for (int j = 0; j < aux1.size(); j++)
    {
        if (j < offset1)
        {
            feedAux.emplace_back(aux1[j]);
        }
        else if (j > offset1 + foldSizeC)
        {
            feedAux.emplace_back(aux1[j]);
        }
        else
        {
            m_vec_validation.emplace_back(aux1[j]);
            m_vec_ConfMatrix.emplace_back(2);
        }
    }
    m_vec_trainingdt.emplace_back(feedAux);

    cout << "Validation Images:" <<endl;
    for (String s : m_vec_validation)
    {
        cout << "Val : "<< s.c_str() << endl;
    }
    cout << "Training Images:" << endl;
    for (vector<String> vec : m_vec_trainingdt)
    {
        for (String s : vec)
        {
            cout << "Trai : " << s.c_str() << endl;;
        }
    }
    cout << "[";
    for (int size = 0, line = 0  ; size < m_vec_ConfMatrix.size(); size++, line++)
    {
        cout<< " " << m_vec_ConfMatrix[size] << " ";

    }
    cout << "]" << endl;
}

