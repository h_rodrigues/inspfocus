#include "stdafx.h"
#include "classifier.h"

#include <iostream>
#include <fstream>
#include <direct.h>
#include <ctime>
#include <stdio.h>
#include <climits>

#include <opencv2/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>

#include "PConfig.h"

#define MIN_BEST_MATCHES 10

using namespace cv;
using namespace ml;
using namespace cv::xfeatures2d;

using namespace std;

Mat canny_dst;
Mat canny_detected_edges;

int canny_lowThreshold=0;
int canny_ratio =3;
int canny_kernel_size =3;


Classifier::Classifier()
{
    m_isConfMatix = false;
}

Classifier::Classifier(bool isConfMatrix, bool isFolds, int folds)
{
    m_console  = nullptr;
//    m_CrossVal = nullptr;

    m_isConfMatix  = isConfMatrix;
    m_isCrossValid = isFolds;
    if (isFolds)
    {
        m_folds = folds;
    }
}

Classifier::~Classifier()
{

}


Ptr<SVM>& Classifier::Train(uchar type)
{
    int dictionarySize = 1000, 
        retries = 1, 
        flags = KMEANS_RANDOM_CENTERS; // ou KMEANS_PP_CENTERS
    
    vector<KeyPoint> keypoints;

    Mat input,
        dictionary,
        descriptor,
        bowDescriptor,
        labels(0, 1, CV_32SC1),
        featuresUnclustered(0, 0, CV_32F),
        trainingData(0, dictionarySize, CV_32FC1);

    Ptr<ORB> detectorOrb = ORB::create();
    
    //hessianThreshold = 100, nOctaves = 4, nOctaveLayers = 3, extended = false, upright = false -> default
    Ptr<SURF> detectorSURF = SURF::create(200); 

    //int nfeatures = 0, int nOctaveLayers = 3,	double contrastThreshold = 0.04, double edgeThreshold = 10,	double sigma = 1.6
    Ptr<SIFT> detectorSIFT = SIFT::create(0, 3, 0.04, 20, 1.6);

    //Cria��o do SVM
    Ptr<SVM> lSVM = SVM::create();
    
                   //Type             MaxCount  elipson
    TermCriteria tc (TermCriteria::MAX_ITER, 100    , 0.001   ),
                 fim(TermCriteria::MAX_ITER, 1000   , 0.000001);

    BOWKMeansTrainer bowTrainer(dictionarySize, tc, retries, flags);

    //Fun��o vai buscar os caminhos para todas as imagens de treino.
    LoadImages();

#ifdef DEBUG
    cout << "Come�ar a extrair os Centroids" << endl;
    //m_console->Output();
#else
    MessageBox(nullptr, "Come�ar a extrair os Centroids", MB_ICONINFORMATION | MB_OK);
#endif // DEBUG

    Ptr<TrainData> tDat;
    Ptr<FeatureDetector> detector;
    Ptr<DescriptorMatcher> matcher;
    Ptr<DescriptorExtractor> extractor;

    for (size_t i = 0; i < m_vecImagesTreino.size(); i++)
    {
        cout << "Centroid imagem ->"<< i << endl;
        //m_console->Output("Centroid imagem -> %d \n", i);
        input = imread(m_vecImagesTreino[i], IMREAD_GRAYSCALE);

        switch (type)
        {
        case kCla_DENSE:
        {
            detectorSIFT->compute(input, Dense_kp(input), descriptor);
        }break;
        case kCla_ORB:
        {
            detectorOrb->detect(input, keypoints);
            detectorOrb->compute(input, keypoints, descriptor);
        }break;
        case kCla_SURF:
        {
            detectorSURF->detect(input, keypoints);
            detectorSURF->compute(input, keypoints, descriptor);
        }break;
        case kCla_SIFT:
        {
            detectorSIFT->detect(input, keypoints);
            detectorSIFT->compute(input, keypoints, descriptor);
        }break;
        }

        featuresUnclustered.push_back(descriptor);
    }

#ifdef DEBUG
    cout << "Clustering imagens." << endl;
    m_console->Output();
#else
    MessageBox(nullptr, "Clustering imagens.", MB_ICONINFORMATION | MB_OK);
#endif // DEBUG

    switch (type)
    {
        case kCla_DENSE :
        {
            dictionary = bowTrainer.cluster(featuresUnclustered);

            matcher = DescriptorMatcher::create("FlannBased");
            //detector = SIFT::create();
            extractor = SIFT::create();
        }break;
        case kCla_ORB:
        {
            Mat featuresUnclusteredF(featuresUnclustered.rows, featuresUnclustered.cols, CV_32F);
            featuresUnclustered.convertTo(featuresUnclusteredF, CV_32F);
            Mat dictionaryF = bowTrainer.cluster(featuresUnclusteredF);
            dictionaryF.convertTo(dictionary, CV_8U);

            matcher = DescriptorMatcher::create("BruteForce-Hamming");
            detector = ORB::create();
            extractor = ORB::create();
        }break;
        case kCla_SURF:
        {
            dictionary = bowTrainer.cluster(featuresUnclustered);

            matcher = DescriptorMatcher::create("FlannBased");
            detector = SURF::create(300);
            extractor = SURF::create(300);
        }break;
        case kCla_SIFT:
        {
            dictionary = bowTrainer.cluster(featuresUnclustered);

            matcher = DescriptorMatcher::create("FlannBased");
            detector = SIFT::create(0, 3, 0.04, 20, 1.6);
            extractor = SIFT::create(0, 3, 0.04, 20, 1.6);
        }break;
    }

#ifdef DEBUG
    cout << "Clustering Feito." << endl;
    //m_console->Output("Clustering Feito.\n");
#else
    MessageBox(nullptr, "Clustering Feito.", MB_ICONINFORMATION | MB_OK);
#endif // DEBUG

    BOWImgDescriptorExtractor bowDE(extractor, matcher);
    bowDE.setVocabulary(dictionary);
    int num = GetNumCategorias();
    for (int j = 1; j <= num; j++) {
        vector<String> list;
        String path = GetPathToTrain();
        path += to_string(j);
        glob(path, list, false);

        for (size_t i = 0; i < list.size(); i++) {

            cout << "BOW Compute | Categoria -> " + to_string(j) + " Imagem -> " + to_string(i) << " Tdata size -> " << trainingData.size() << endl;

            Mat img = imread(list[i], IMREAD_GRAYSCALE);
            if (type == kCla_DENSE)
                bowDE.compute(img, Dense_kp(input), bowDescriptor);
            else {
                detector->detect(img, keypoints);
                bowDE.compute(img, keypoints, bowDescriptor);
            }
            if (!bowDescriptor.empty()) {
                trainingData.push_back(bowDescriptor);
                labels.push_back(j);
            }
        }
    }

    lSVM->setTermCriteria(fim);
    tDat = TrainData::create(trainingData, ROW_SAMPLE, labels);
    cout << "A treinar SVM" << endl;
    bool res = lSVM->trainAuto(tDat, 20);

    string save_spot = GetPathToSVM();
    switch (type)
    {
        case kCla_DENSE:
        {
            save_spot += m_SvmName;
            save_spot += "_SVM_DENSE.xml";
        }break;
        case kCla_ORB:
        {
            save_spot += m_SvmName;
            save_spot += "_SVM_ORB.xml";
        }break;
        case kCla_SURF:
        {
            save_spot += m_SvmName;
            save_spot += "_SVM_SURF.xml";
        }break;
        case kCla_SIFT:
        {
            save_spot += m_SvmName;
            save_spot += "_SVM_SIFT.xml";
        }break;
    }
    lSVM->save(save_spot);

    Mat voc = bowDE.getVocabulary();

    save_spot = GetPathToSVM();
    switch (type)
    {
        case kCla_DENSE:
        {
            save_spot += m_SvmName;
            save_spot += "_SVM_DENSE.xml";
        }break;
        case kCla_ORB:
        {
            save_spot += m_SvmName;
            save_spot += "_SVM_ORB.xml";
        }break;
        case kCla_SURF:
        {
            save_spot += m_SvmName;
            save_spot += "_SVM_SURF.xml";
        }break;
        case kCla_SIFT:
        {
            save_spot += m_SvmName;
            save_spot += "_SVM_SIFT.xml";
        }break;
    }
    FileStorage fs(save_spot, FileStorage::WRITE);
    fs << "vocabulary" << voc;
    fs.release();

#ifdef DEBUG
    m_console->Output("Concluido treino de SVM com um total de %d imagens.\n", m_vecImagesTreino.size());
#else
    MessageBox(nullptr, "Concluido treino de SVM.", MB_ICONINFORMATION | MB_OK);
#endif

    m_vecSVMs.push_back(lSVM);
    return m_vecSVMs.back();
}

int Classifier::Classify(uchar type)
{
    return NormalClassify(type);
}


int Classifier::NormalClassify(uchar type)
{
    vector<KeyPoint> keypoint;
    vector<int> pred_labels;
    Mat bowDescriptor, img, img_noclahe;
    ofstream textfile;
    string result_path = GetPathToResult();

    Ptr<FeatureDetector> detector;
    Ptr<DescriptorMatcher> matcher;
    Ptr<DescriptorExtractor> extractor;
    Ptr<SVM> my_svm;
    Ptr<CLAHE> clahe = createCLAHE();

    switch (type)
    {
        case kCla_DENSE:
            {
                matcher = DescriptorMatcher::create("FlannBased");
                //detector = SIFT::create();
                extractor = SIFT::create();

                result_path += "Results_DENSE.txt";
            }break;
        case kCla_ORB:
            {
                matcher = DescriptorMatcher::create("BruteForce-Hamming");
                detector = ORB::create();
                extractor = ORB::create();

                result_path += "Results_ORB.txt";
            }break;
        case kCla_SURF:
            {
                matcher = DescriptorMatcher::create("FlannBased");
                detector = SURF::create();
                extractor = SURF::create();

                result_path += "Results_SURF.txt";
            }break;
        case kCla_SIFT:
            {
                matcher = DescriptorMatcher::create("FlannBased");
                detector = SIFT::create(0, 3, 0.04, 20, 1.6);
                extractor = SIFT::create(0, 3, 0.04, 20, 1.6);

                result_path += "Results_SIFT.txt";
            }break;
        default:
            break;
    }

    BOWImgDescriptorExtractor bowDE(extractor, matcher);

    my_svm = Load(type, bowDE);

    textfile.open(result_path);

    for (int i = 0; i < m_vecImagesTeste.size(); i++) {
        cout << "Classificando imagem -> " + m_vecImagesTeste[i] << endl;

        img_noclahe = imread(m_vecImagesTeste[i], IMREAD_GRAYSCALE);

        clahe->apply(img_noclahe, img);

        //img = imread(lista_imagens_teste[i], CV_LOAD_IMAGE_GRAYSCALE);

        if (type == kCla_DENSE)
            bowDE.compute(img, Dense_kp(img), bowDescriptor);
        else {
            detector->detect(img, keypoint);
            bowDE.compute(img, keypoint, bowDescriptor);
        }

        int response = my_svm->predict(bowDescriptor);
        pred_labels.push_back(response);

        textfile << m_vecImagesTeste[i] << " | " << response << endl;
    }

    textfile.close();

    if (m_isConfMatix)
      //  confusion_matrix(pred_labels);

    return 0;
}
Ptr<SVM>& Classifier::Load(uchar type, BOWImgDescriptorExtractor &bowDE, int fold)
{
    Ptr<SVM> lSVM = SVM::create();

    string svm_path = GetPathToSVM();
    string voc_path = GetPathToSVM();

    switch (type)
    {
    case kCla_DENSE:
    {
        svm_path += m_SvmName;
        svm_path += "_SVM_DENSE_" + to_string(fold) + ".xml";

        voc_path += m_SvmName;
        voc_path += "_VOC_DENSE_" + to_string(fold) + ".xml";

    }break;
    case kCla_ORB:
    {
        svm_path += m_SvmName;
        svm_path += "_SVM_ORB_" + to_string(fold) + ".xml";

        voc_path += m_SvmName;
        voc_path += "_VOC_ORB_" + to_string(fold) + ".xml";
    }break;
    case kCla_SURF:
    {
        svm_path += m_SvmName;
        svm_path += "_SVM_SURF_" + to_string(fold) + ".xml";

        voc_path += m_SvmName;
        voc_path += "_VOC_SURF_" + to_string(fold) + ".xml";
    }break;
    case kCla_SIFT:
    {
        svm_path += m_SvmName;
        svm_path += "_SVM_SIFT_" + to_string(fold) + ".xml";

        voc_path += m_SvmName;
        voc_path += "_VOC_SIFT_" + to_string(fold) + ".xml";
    }break;
    }

    lSVM = SVM::load(svm_path);
    if (lSVM->isTrained()) {
        Mat vocabulary;
        FileStorage fs(voc_path, FileStorage::READ);
        fs["vocabulary"] >> vocabulary;
        fs.release();

        bowDE.setVocabulary(vocabulary);
    }

    return lSVM;
}


Ptr<SVM>& Classifier::Load(uchar type, BOWImgDescriptorExtractor &bowDE)
{	
    Ptr<SVM> lSVM = SVM::create();
    
    string svm_path = GetPathToSVM();
    string voc_path = GetPathToSVM();
    
    switch (type)
    {
        case kCla_DENSE :
        {
            svm_path += "_SVM_DENSE.xml";
            voc_path += "_VOC_DENSE.xml";
        }break;
        case kCla_ORB:
        {
            svm_path += "_SVM_ORB.xml";
            voc_path += "_VOC_ORB.xml";
        }break;
        case kCla_SURF:
        {
            svm_path += "_SVM_SURF.xml";
            voc_path += "_VOC_SURF.xml";
        }break;
        case kCla_SIFT:
        {
            svm_path += "_SVM_SIFT.xml";
            voc_path += "_VOC_SIFT.xml";
        }break;
    }
    lSVM = SVM::load(svm_path);
    if (lSVM->isTrained()) {
        Mat vocabulary;
        FileStorage fs(voc_path, FileStorage::READ);
        fs["vocabulary"] >> vocabulary;
        fs.release();

        bowDE.setVocabulary(vocabulary);
    }

    m_vecSVMs.push_back(lSVM);
    return m_vecSVMs.back();
}

void Classifier::LoadImages()
{
    int num_cat = GetNumCategorias();
    vector<String> auxLista;
    String auxConv;
    for (int i = 1; i <= num_cat; i++) {
        auxConv = GetPathToTrain();
        auxConv += to_string(i);
        glob(auxConv, auxLista, false);

        for (size_t j = 0; j < auxLista.size(); j++)
            m_vecImagesTreino.push_back(auxLista[j]);
    }
}

void Classifier::LoadTesteImages()
{
    vector<String> auxLista;
    String auxConv;

    auxConv = GetPathToTest();
    glob(auxConv, auxLista, false);

    for (size_t j = 0; j < auxLista.size(); j++)
        m_vecImagesTeste.push_back(auxLista[j]);
}

vector<KeyPoint> Classifier::Dense_kp(Mat image) {
    int step = 25; // 10 pixels spacing between kp's

    vector<KeyPoint> kps;
    for (int i = step; i < image.rows - step; i += step)
    {
        for (int j = step; j < image.cols - step; j += step)
        {
            // x,y,radius
            kps.push_back(KeyPoint(float(j), float(i), float(step)));
        }
    }
    return kps;
}

void Classifier::SetSVMFileName(const char* name)
{
    m_SvmName = name;
}

void Classifier::SetConfMatrix(bool active)
{
    m_isConfMatix = active;
}

Mat Classifier::Subtract(cv::Mat img1, cv::Mat img2)
{
    Mat img_out;
    subtract(img1, img2, img_out);

    namedWindow("subtract", WINDOW_NORMAL);
    imshow("subtract", img_out);

    return img_out;
}

Mat Classifier::Subtract(const char * img_1, const char * img_2)
{

    Mat img1,img2,convert,resized;
    Mat canny_output;
    Mat canny_dst;
    Mat canny_detected_edges;

    int canny_lowThreshold = 0;
    int canny_ratio = 3;
    int canny_kernel_size = 3;


    img1 = imread(img_1, IMREAD_GRAYSCALE);
    img2 = imread(img_2, IMREAD_GRAYSCALE);

    subtract(img1        , img2, canny_output);
    subtract(canny_output, img2, convert);
    if (canny_output.empty())
    {
        cout << "No output values" << endl;

        return canny_output;
    }
    namedWindow("Origianl Image 1", WINDOW_NORMAL);
    imshow("Origianl Image 1", img1);

    namedWindow("Origianl Image 2", WINDOW_NORMAL);
    imshow("Origianl Image 2", img2);

    namedWindow("subtract", WINDOW_NORMAL);
    imshow("subtract", canny_output);
    //namedWindow("subtract2", WINDOW_NORMAL);
    //imshow("subtract2", convert);

    resize(canny_output, resized, Size(800, 600));

    canny_dst.create(resized.size(), resized.type());
    resized.copyTo(canny_output);

    return canny_output;
}

cv::Mat Classifier::CannyThreshold(int minthreshold,Mat& img)
{
    Mat canny_dst;
    Mat canny_detected_edges;

    canny_dst.create(img.size(), img.type());

    canny_lowThreshold = 0;
    canny_kernel_size = 5;

    /// Reduce noise with a kernel 3x3
    blur(img, canny_detected_edges, Size(3, 3));

    /// Canny detector
    Canny(canny_detected_edges, canny_detected_edges, minthreshold, (canny_lowThreshold)*(canny_ratio),
        canny_kernel_size);

    /// Using Canny's output as a mask, we display our result
    canny_dst = Scalar::all(0);

    img.copyTo(canny_dst, canny_detected_edges);

    namedWindow("Canny", WINDOW_AUTOSIZE);
    imshow("Canny", canny_dst);

    return canny_dst;
}

int Classifier::CrossTrain(int fold, uchar type, std::vector<std::vector<cv::String>>& treino)
{
    int dictionarySize = 1000,
        retries = 1,
        flags = KMEANS_RANDOM_CENTERS; // ou KMEANS_PP_CENTERS

    vector<KeyPoint> keypoints;

    Mat input,
        dictionary,
        descriptor,
        bowDescriptor,
        labels(0, 1, CV_32SC1),
        featuresUnclustered(0, 0, CV_32F),
        trainingData(0, dictionarySize, CV_32FC1);

    Ptr<ORB> detectorOrb = ORB::create();

    //hessianThreshold = 100, nOctaves = 4, nOctaveLayers = 3, extended = false, upright = false -> default
    Ptr<SURF> detectorSURF = SURF::create(300);

    //int nfeatures = 0, int nOctaveLayers = 3,	double contrastThreshold = 0.04, double edgeThreshold = 10,	double sigma = 1.6
    Ptr<SIFT> detectorSIFT = SIFT::create(0, 3, 0.04, 20, 1.6);

    //Cria��o do SVM
    Ptr<SVM> lSVM = SVM::create();

    //Type             MaxCount  elipson
    TermCriteria tc(TermCriteria::MAX_ITER, 100, 0.001),
        fim(TermCriteria::MAX_ITER, 1000, 0.000001);

    BOWKMeansTrainer bowTrainer(dictionarySize, tc, retries, flags);

    //Fun��o vai buscar os caminhos para todas as imagens de treino.
    LoadImages();

#ifdef DEBUG
    cout << "Come�ar a extrair os Centroids" << endl;
    //m_console->Output();
#else
    MessageBox(nullptr, "Come�ar a extrair os Centroids", MB_ICONINFORMATION | MB_OK);
#endif // DEBUG

    Ptr<TrainData> tDat;
    Ptr<FeatureDetector> detector;
    Ptr<DescriptorMatcher> matcher;
    Ptr<DescriptorExtractor> extractor;

    for (int categoria = 0; categoria < treino.size(); categoria++)
    {
        for (String img : treino[categoria])
        {
            cout << "Centroid imagem ->" << img << endl;
            //m_console->Output("Centroid imagem -> %d \n", i);
            input = imread(img, IMREAD_GRAYSCALE);

            switch (type)
            {
            case kCla_DENSE:
            {
                detectorSIFT->compute(input, Dense_kp(input), descriptor);
            }break;
            case kCla_ORB:
            {
                detectorOrb->detect(input, keypoints);
                detectorOrb->compute(input, keypoints, descriptor);
            }break;
            case kCla_SURF:
            {
                detectorSURF->detect(input, keypoints);
                detectorSURF->compute(input, keypoints, descriptor);
            }break;
            case kCla_SIFT:
            {
                detectorSIFT->detect(input, keypoints);
                detectorSIFT->compute(input, keypoints, descriptor);
            }break;
            }

            featuresUnclustered.push_back(descriptor);
        }
    }
        

#ifdef DEBUG
    cout << "Clustering imagens." << endl;
#else
    MessageBox(nullptr, "Clustering imagens.", MB_ICONINFORMATION | MB_OK);
#endif // DEBUG

    switch (type)
    {
    case kCla_DENSE:
    {
        dictionary = bowTrainer.cluster(featuresUnclustered);

        matcher = DescriptorMatcher::create("FlannBased");
        //detector = SIFT::create();
        extractor = SIFT::create();
    }break;
    case kCla_ORB:
    {
        Mat featuresUnclusteredF(featuresUnclustered.rows, featuresUnclustered.cols, CV_32F);
        featuresUnclustered.convertTo(featuresUnclusteredF, CV_32F);
        Mat dictionaryF = bowTrainer.cluster(featuresUnclusteredF);
        dictionaryF.convertTo(dictionary, CV_8U);

        matcher = DescriptorMatcher::create("BruteForce-Hamming");
        detector = ORB::create();
        extractor = ORB::create();
    }break;
    case kCla_SURF:
    {
        dictionary = bowTrainer.cluster(featuresUnclustered);

        matcher = DescriptorMatcher::create("FlannBased");
        detector = SURF::create(300);
        extractor = SURF::create(300);
    }break;
    case kCla_SIFT:
    {
        dictionary = bowTrainer.cluster(featuresUnclustered);

        matcher = DescriptorMatcher::create("FlannBased");
        detector = SIFT::create(0, 3, 0.04, 20, 1.6);
        extractor = SIFT::create(0, 3, 0.04, 20, 1.6);
    }break;
    }

#ifdef DEBUG
    cout << "Clustering Feito." << endl;
    //m_console->Output("Clustering Feito.\n");
#else
    MessageBox(nullptr, "Clustering Feito.", MB_ICONINFORMATION | MB_OK);
#endif // DEBUG

    BOWImgDescriptorExtractor bowDE(extractor, matcher);
    bowDE.setVocabulary(dictionary);

    int num_categoria = 1;
    for (vector<String> categ : treino)
    {
        for (String img_p : categ)
        {
            cout << "BOW Compute | Categoria -> "<< num_categoria << " Imagem -> " << img_p << " Tdata size -> " << trainingData.size() << endl;

            Mat img = imread(img_p, IMREAD_GRAYSCALE);
            if (type == kCla_DENSE)
                bowDE.compute(img, Dense_kp(input), bowDescriptor);
            else {
                detector->detect(img, keypoints);
                bowDE.compute(img, keypoints, bowDescriptor);
            }
            if (!bowDescriptor.empty()) {
                trainingData.push_back(bowDescriptor);
                labels.push_back(num_categoria);
            }

        }
        num_categoria++;
    }

    lSVM->setTermCriteria(fim);
    tDat = TrainData::create(trainingData, ROW_SAMPLE, labels);
    cout << "A treinar SVM" << endl;
    bool res = lSVM->trainAuto(tDat, 20);

    string save_spot = GetPathToSVM();
    switch (type)
    {
    case kCla_DENSE:
    {
        save_spot += m_SvmName;
        save_spot += "_SVM_DENSE_" + to_string(fold) + ".xml";
    }break;
    case kCla_ORB:
    {
        save_spot += m_SvmName;
        save_spot += "_SVM_ORB_" + to_string(fold) + ".xml";
    }break;
    case kCla_SURF:
    {
        save_spot += m_SvmName;
        save_spot += "_SVM_SURF_" + to_string(fold) + ".xml";
    }break;
    case kCla_SIFT:
    {
        save_spot += m_SvmName;
        save_spot += "_SVM_SIFT_" + to_string(fold) + ".xml";
    }break;
    }
    lSVM->save(save_spot);

    Mat voc = bowDE.getVocabulary();

    save_spot = GetPathToSVM();
    switch (type)
    {
    case kCla_DENSE:
    {
        save_spot += m_SvmName;
        save_spot += "_VOC_DENSE_"+to_string(fold)+".xml";
    }break;
    case kCla_ORB:
    {
        save_spot += m_SvmName;
        save_spot += "_VOC_ORB_" + to_string(fold) + ".xml";
    }break;
    case kCla_SURF:
    {
        save_spot += m_SvmName;
        save_spot += "_VOC_SURF_" + to_string(fold) + ".xml";
    }break;
    case kCla_SIFT:
    {
        save_spot += m_SvmName;
        save_spot += "_VOC_SIFT_" + to_string(fold) + ".xml";
    }break;
    }
    FileStorage fs(save_spot, FileStorage::WRITE);
    fs << "vocabulary" << voc;
    fs.release();

#ifdef DEBUG
    cout << "Concluido treino de SVM com um total de" << treino.size() << "imagens."<< endl;
#else
    MessageBox(nullptr, "Concluido treino de SVM.", MB_ICONINFORMATION | MB_OK);
#endif
    lSVM.release();
    
    return 1;
}

int Classifier::CrossValid(int fold, uchar type, std::vector<cv::String>& valida, std::vector<int>& conf)
{
    vector<KeyPoint> keypoint;
    vector<int> pred_labels;
    Mat bowDescriptor, img, img_noclahe;
    ofstream textfile;
    string result_path = GetPathToResult();

    Ptr<FeatureDetector> detector;
    Ptr<DescriptorMatcher> matcher;
    Ptr<DescriptorExtractor> extractor;
    Ptr<SVM> lSVM;
    Ptr<CLAHE> clahe = createCLAHE();

    switch (type)
    {
    case kCla_DENSE:
    {
        matcher = DescriptorMatcher::create("FlannBased");
        //detector = SIFT::create();
        extractor = SIFT::create();
        result_path += m_SvmName;
        result_path += "_Results_DENSE_" + to_string(fold) + ".txt";
    }break;
    case kCla_ORB:
    {
        matcher = DescriptorMatcher::create("BruteForce-Hamming");
        detector = ORB::create();
        extractor = ORB::create();
        result_path += m_SvmName;
        result_path += "_Results_ORB_" + to_string(fold) + ".txt";
    }break;
    case kCla_SURF:
    {
        matcher = DescriptorMatcher::create("FlannBased");
        detector = SURF::create();
        extractor = SURF::create();
        result_path += m_SvmName;
        result_path += "_Results_SURF_" + to_string(fold) + ".txt";
    }break;
    case kCla_SIFT:
    {
        matcher = DescriptorMatcher::create("FlannBased");
        detector = SIFT::create(0, 3, 0.04, 20, 1.6);
        extractor = SIFT::create(0, 3, 0.04, 20, 1.6);
        result_path += m_SvmName;
        result_path += "_Results_SIFT_" + to_string(fold) + ".txt";
    }break;
    default:
        break;
    }

    BOWImgDescriptorExtractor bowDE(extractor, matcher);


    string svm_path = GetPathToSVM();
    string voc_path = GetPathToSVM();

    switch (type)
    {
    case kCla_DENSE:
    {
        svm_path += m_SvmName;
        svm_path += "_SVM_DENSE_" + to_string(fold) + ".xml";

        voc_path += m_SvmName;
        voc_path += "_VOC_DENSE_" + to_string(fold) + ".xml";

    }break;
    case kCla_ORB:
    {
        svm_path += m_SvmName;
        svm_path += "_SVM_ORB_" + to_string(fold) + ".xml";

        voc_path += m_SvmName;
        voc_path += "_VOC_ORB_" + to_string(fold) + ".xml";
    }break;
    case kCla_SURF:
    {
        svm_path += m_SvmName;
        svm_path += "_SVM_SURF_" + to_string(fold) + ".xml";

        voc_path += m_SvmName;
        voc_path += "_VOC_SURF_" + to_string(fold) + ".xml";
    }break;
    case kCla_SIFT:
    {
        svm_path += m_SvmName;
        svm_path += "_SVM_SIFT_" + to_string(fold) + ".xml";

        voc_path += m_SvmName;
        voc_path += "_VOC_SIFT_" + to_string(fold) + ".xml";
    }break;
    }

    lSVM = SVM::load(svm_path);
    if (lSVM->isTrained()) {
        Mat vocabulary;
        FileStorage fs(voc_path, FileStorage::READ);
        fs["vocabulary"] >> vocabulary;
        fs.release();

        bowDE.setVocabulary(vocabulary);
    }

    textfile.open(result_path);

    for (int i = 0; i < valida.size(); i++) {
        cout << "Classificando imagem -> " + valida[i] << endl;

        img_noclahe = imread(valida[i], IMREAD_GRAYSCALE);

        clahe->apply(img_noclahe, img);

        //img = imread(lista_imagens_teste[i], CV_LOAD_IMAGE_GRAYSCALE);

        if (type == kCla_DENSE)
            bowDE.compute(img, Dense_kp(img), bowDescriptor);
        else {
            detector->detect(img, keypoint);
            bowDE.compute(img, keypoint, bowDescriptor);
        }

        int response = lSVM->predict(bowDescriptor);
        pred_labels.push_back(response);

        textfile << valida[i] << " | " << response << "Fold" << fold << endl;
    }

    textfile.close();

    if (m_isConfMatix)
         confusion_matrix(fold,pred_labels,conf);

    lSVM.release();
    clahe.release();

    return 0;
}

void Classifier::confusion_matrix(int fold, std::vector<int>& pred_labels, std::vector<int>& true_labels) {

    std::fstream fs;
    fs.open("Confusion_Matrix_"+m_SvmName+".txt", std::fstream::in | std::fstream::out | std::fstream::app);

    if (true_labels.size() != pred_labels.size()) {
        cout << "Conjunto de teste diferente." << endl << endl;
        return;
    }

    cout << "Matriz de Confus�o"<< fold << endl << endl;
    fs   << "Matriz de Confus�o"<< fold << endl << endl;

    int confusion  [NUM_CATEGORIAS][NUM_CATEGORIAS] = {};
    int totalsPred [NUM_CATEGORIAS] = {};
    int totalsLabel[NUM_CATEGORIAS] = {};

    for (int i = 0; i < true_labels.size(); i++)
        confusion[true_labels.at(i) - 1][pred_labels.at(i) - 1] += 1;

    for (int i = 0; i < NUM_CATEGORIAS; i++) {
        for (int j = 0; j < NUM_CATEGORIAS; j++) {
            totalsPred [i] += confusion[j][i];
            totalsLabel[j] += confusion[j][i];
        }
    }

    cout << "      Label | ";
    fs   << "      Label | ";
    for (int i = 0; i < NUM_CATEGORIAS; i++) {
        if (i < 10)
        {
            cout << to_string(i + 1) + "  ";
            fs   << to_string(i + 1) + "  ";
        }
        else
        {
            cout << to_string(i + 1) + " ";
            fs   << to_string(i + 1) + "  ";
        }
    }
    cout << "| Total" << endl << "----------------";
    fs   << "| Total" << endl << "----------------";

    for (int i = 0; i < NUM_CATEGORIAS + 2; i++) {
        cout << "---";
        fs   << "---";
    }
    cout << endl;
    fs   << endl;

    for (int i = 0; i < NUM_CATEGORIAS + 1; i++) {
        if (i < NUM_CATEGORIAS) {
            cout << "Predicted " + to_string(i + 1) + " | ";
            fs   << "Predicted " + to_string(i + 1) + " | ";

            for (int j = 0; j < NUM_CATEGORIAS; j++) {
                if (confusion[j][i] < 10)
                {
                    cout << confusion[j][i] << "  ";
                    fs   << confusion[j][i] << "  ";
                }
                else
                {
                    cout << confusion[j][i] << " ";
                    fs   << confusion[j][i] << "  ";
                }
            }
            cout << "| " + to_string(totalsPred[i]);
            fs   << "| " + to_string(totalsPred[i]);
        }
        else {
            cout << "----------------";
            fs   << "----------------";
            for (int i = 0; i < NUM_CATEGORIAS + 2; i++)
            {
                cout << "---";
                fs   << "---";
            }

            cout << endl << "Total       | ";
            fs   << endl << "Total       | ";
            for (int j = 0; j < NUM_CATEGORIAS; j++) {
                if (totalsLabel[j] < 10)
                {
                    cout << totalsLabel[j] << "  ";
                    fs   << totalsLabel[j] << "  ";
                }
                else
                {
                    cout << totalsLabel[j] << " ";
                    fs   << totalsLabel[j] << " ";
                }
            }
            cout << "| ";
            fs   << "| ";
        }
        cout << endl;
        fs   << endl;
    }
    cout << endl;
    fs   << endl;

    int tp = 0, tn = 0, fp = 0, fn = 0;

    for (int i = 0; i < NUM_CATEGORIAS; i++) {
        double recall, precision;
        if (totalsLabel[i] == 0)
            recall = 0;
        else recall = confusion[i][i] / (double)totalsLabel[i];

        if (totalsPred[i] == 0)
            precision = 0;
        else precision = confusion[i][i] / (double)totalsPred[i];

        cout << "Classe " + to_string(i + 1) + " | Recall -> " + to_string(recall) + " Precision -> " + to_string(precision) << endl;
        fs   << "Classe " + to_string(i + 1) + " | Recall -> " + to_string(recall) + " Precision -> " + to_string(precision) << endl;

        tp += confusion[i][i];

    }

    cout << endl << to_string(tp) + "/" + to_string(true_labels.size()) << endl;
    fs   << endl << to_string(tp) + "/" + to_string(true_labels.size()) << endl;

    cout << endl;
    fs   << endl;

    fs.close();
}

void Classifier::SiftCompare(cv::Mat img_object, cv::Mat img_scene)
{
    Ptr<SIFT> detector = SIFT::create();

    vector<KeyPoint> keypoints_scene, keypoints_object;
    Mat descriptor_scene, descriptor_object, img_matches;

    detector->detectAndCompute(img_object, Mat(), keypoints_object, descriptor_object);
    detector->detectAndCompute(img_scene, Mat(), keypoints_scene, descriptor_scene);

    FlannBasedMatcher flann;
    vector<vector<DMatch>> matches;
    vector<DMatch> good_matches;

    flann.knnMatch(descriptor_object, descriptor_scene, matches, 2);

    for (int i = 0; i < matches.size(); ++i) {
        const float ratio = 0.7; //  Lowe's ratio
        if (matches[i][0].distance < ratio * matches[i][1].distance)
            good_matches.push_back(matches[i][0]);
    }

    drawMatches(img_object, keypoints_object, img_scene, keypoints_scene,
        good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
        vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

    /*cv::imshow("Good Matches & Object detection", img_matches);
    waitKey(0);*/

    if (good_matches.size() >= MIN_BEST_MATCHES) {

        std::vector<Point2f> obj;
        std::vector<Point2f> scene;

        for (int i = 0; i < good_matches.size(); i++)
        {
            //-- Get the keypoints from the good matches
            obj.push_back(keypoints_object[good_matches[i].queryIdx].pt);
            scene.push_back(keypoints_scene[good_matches[i].trainIdx].pt);
        }

        Mat H = findHomography(obj, scene, RANSAC);

        if (!niceHomography(H)) {
            cout << "Bad homography" << endl;
            //return 0; 
        }

        //const double det = H.at<double>(0, 0) * H.at<double>(1, 1) - H.at<double>(1, 0) * H.at<double>(0, 1);

        //if ((fabs(det) > (double)N) || (fabs(det) < (1.0 / (double)N))) {
        //	cout << "Bad homography" << endl;
        //	//return 0; // bad homography
        //}
        //	

        //-- Get the corners from the image_1 ( the object to be "detected" )
        std::vector<Point2f> obj_corners(4);
        obj_corners[0] = Point(0, 0); obj_corners[1] = Point(img_object.cols, 0);
        obj_corners[2] = Point(img_object.cols, img_object.rows); obj_corners[3] = Point(0, img_object.rows);
        std::vector<Point2f> scene_corners(4);

        perspectiveTransform(obj_corners, scene_corners, H);

        //-- Draw lines between the corners (the mapped object in the scene - image_2 )
        line(img_matches, scene_corners[0] + Point2f(img_object.cols, 0), scene_corners[1] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
        line(img_matches, scene_corners[1] + Point2f(img_object.cols, 0), scene_corners[2] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
        line(img_matches, scene_corners[2] + Point2f(img_object.cols, 0), scene_corners[3] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
        line(img_matches, scene_corners[3] + Point2f(img_object.cols, 0), scene_corners[0] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
    }
    else
        cout << "Not enough good matches -> " << good_matches.size() << endl;

    _mkdir("ResultsSIFT");
    imwrite(".\\ResultsSIFT\\result.png", img_matches);
    //-- Show detected matches
    imshow("Good Matches & Object detection", img_matches);

    waitKey(0);
}

void Classifier::OrbComapre(cv::Mat img_object, cv::Mat img_scene)
{
    // ORB + BRUTE FORCE

    vector<KeyPoint> keypoints_scene, keypoints_object;
    Mat descriptor_scene, descriptor_object, img_matches;
    vector<DMatch> good_matches, matches;

    Ptr<ORB> detector = ORB::create();

    detector->detectAndCompute(img_object, Mat(), keypoints_object, descriptor_object);
    detector->detectAndCompute(img_scene, Mat(), keypoints_scene, descriptor_scene);

    BFMatcher matcher(NORM_HAMMING2);

    matcher.match(descriptor_object, descriptor_scene, matches);

    double max_dist = 0; double min_dist = INT_MAX;

    for (int i = 0; i < descriptor_object.rows; i++)
    {
        double dist = matches[i].distance;
        if (dist < min_dist) min_dist = dist;
        if (dist > max_dist) max_dist = dist;
    }

    printf("-- Max dist : %f \n", max_dist);
    printf("-- Min dist : %f \n", min_dist);

    for (int i = 0; i < descriptor_object.rows; i++)
    {
        if (matches[i].distance < 2 * min_dist)
        {
            good_matches.push_back(matches[i]);
        }
    }

    drawMatches(img_object, keypoints_object, img_scene, keypoints_scene,
        good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
        vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

    if (good_matches.size() >= MIN_BEST_MATCHES) {

        std::vector<Point2f> obj;
        std::vector<Point2f> scene;

        for (int i = 0; i < good_matches.size(); i++)
        {
            //-- Get the keypoints from the good matches
            obj.push_back(keypoints_object[good_matches[i].queryIdx].pt);
            scene.push_back(keypoints_scene[good_matches[i].trainIdx].pt);
        }

        Mat H = findHomography(obj, scene, RANSAC);

        if (!niceHomography(H)) {
            cout << "Bad homography" << endl;
            //return 0; 
        }

        //-- Get the corners from the image_1 ( the object to be "detected" )
        std::vector<Point2f> obj_corners(4);
        obj_corners[0] = cv::Point(0, 0); obj_corners[1] = cv::Point(img_object.cols, 0);
        obj_corners[2] = cv::Point(img_object.cols, img_object.rows); obj_corners[3] = cv::Point(0, img_object.rows);
        std::vector<Point2f> scene_corners(4);

        perspectiveTransform(obj_corners, scene_corners, H);

        //-- Draw lines between the corners (the mapped object in the scene - image_2 )
        line(img_matches, scene_corners[0] + Point2f(img_object.cols, 0), scene_corners[1] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
        line(img_matches, scene_corners[1] + Point2f(img_object.cols, 0), scene_corners[2] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
        line(img_matches, scene_corners[2] + Point2f(img_object.cols, 0), scene_corners[3] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
        line(img_matches, scene_corners[3] + Point2f(img_object.cols, 0), scene_corners[0] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
    }
    else
        cout << "Not enough good matches -> " << good_matches.size() << endl;

    _mkdir("ResultsORB");
    imwrite(".\\ResultsORB\\result.png", img_matches);
    //-- Show detected matches
    imshow("Good Matches & Object detection", img_matches);

    waitKey(0);

}

void Classifier::SurfCompare(cv::Mat img_object, cv::Mat img_scene)
{
    //-- Step 1: Detect the keypoints using SURF Detector
    int minHessian = 400;

    Ptr<SURF> detector = SURF::create();
    detector->setHessianThreshold(minHessian);
    std::vector<KeyPoint> keypoints_object, keypoints_scene;

    //-- Step 2: Calculate descriptors (feature vectors)
    Mat descriptors_object, descriptors_scene;
    detector->detectAndCompute(img_object, Mat(), keypoints_object, descriptors_object);
    detector->detectAndCompute(img_scene, Mat(), keypoints_scene, descriptors_scene);

    std::cout << "descriptor 1 size: " << descriptors_object.total() << endl;
    std::cout << "descriptor 2 size: " << descriptors_scene.total() << endl;


    //-- Step 3: Matching descriptor vectors using FLANN matcher
    FlannBasedMatcher matcher;
    std::vector< DMatch > matches;
    matcher.match(descriptors_object, descriptors_scene, matches);

    double max_dist = 0; double min_dist = 100;

    //-- Quick calculation of max and min distances between keypoints
    for (int i = 0; i < descriptors_object.rows; i++)
    {
        double dist = matches[i].distance;
        if (dist < min_dist) min_dist = dist;
        if (dist > max_dist) max_dist = dist;
    }

    printf("-- Max dist : %f \n", max_dist);
    printf("-- Min dist : %f \n", min_dist);

    //-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
    std::vector< DMatch > good_matches;

    for (int i = 0; i < descriptors_object.rows; i++)
    {
        if (matches[i].distance < 4 * min_dist)
        {
            good_matches.push_back(matches[i]);
        }
    }

    Mat img_matches;
    drawMatches(img_object, keypoints_object, img_scene, keypoints_scene,
        good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
        vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

    printf("-- Good matches : %i \n", good_matches.size());
    //-- Localize the object
    std::vector<Point2f> obj;
    std::vector<Point2f> scene;

    for (int i = 0; i < good_matches.size(); i++)
    {
        //-- Get the keypoints from the good matches
        obj.push_back(keypoints_object[good_matches[i].queryIdx].pt);
        scene.push_back(keypoints_scene[good_matches[i].trainIdx].pt);
    }

    //Mat H = findHomography( obj, scene, RANSAC 
    Mat H = findHomography(obj, scene, RANSAC);


    //-- Get the corners from the image_1 ( the object to be "detected" )
    std::vector<Point2f> obj_corners(4);
    obj_corners[0] = cv::Point(0, 0); obj_corners[1] = cv::Point(img_object.cols, 0);
    obj_corners[2] = cv::Point(img_object.cols, img_object.rows); obj_corners[3] = cv::Point(0, img_object.rows);
    std::vector<Point2f> scene_corners(4);

    perspectiveTransform(obj_corners, scene_corners, H);

    //-- Draw lines between the corners (the mapped object in the scene - image_2 )

    line(img_matches, scene_corners[0] + Point2f(img_object.cols, 0), scene_corners[1] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
    line(img_matches, scene_corners[1] + Point2f(img_object.cols, 0), scene_corners[2] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
    line(img_matches, scene_corners[2] + Point2f(img_object.cols, 0), scene_corners[3] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
    line(img_matches, scene_corners[3] + Point2f(img_object.cols, 0), scene_corners[0] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);

    //-- Show detected matches
    imshow("Good Matches & Object detection", img_matches);

    waitKey(0);
}

bool Classifier::niceHomography(Mat H)
{
    const double det = H.at<double>(0, 0) * H.at<double>(1, 1) - H.at<double>(1, 0) * H.at<double>(0, 1);
    if (det < 0)
        return false;

    const double N1 = sqrt(H.at<double>(0, 0) * H.at<double>(0, 0) + H.at<double>(1, 0) * H.at<double>(1, 0));
    if (N1 > 4 || N1 < 0.1)
        return false;

    const double N2 = sqrt(H.at<double>(0, 1) * H.at<double>(0, 1) + H.at<double>(1, 1) * H.at<double>(1, 1));
    if (N2 > 4 || N2 < 0.1)
        return false;

    const double N3 = sqrt(H.at<double>(2, 0) * H.at<double>(2, 0) + H.at<double>(2, 1) * H.at<double>(2, 1));
    if (N3 > 0.002)
        return false;

    return true;
}

Mat Classifier::SobelEdgeDetection(cv::Mat& img)
{
    Mat src_gray;
    Mat grad;

    if (!img.data)
    {
        return grad;
    }

    int scale = 1;
    int delta = 0;
    int ddepth = CV_16S;

    GaussianBlur(img, img, Size(3, 3), 0, 0, BORDER_DEFAULT);

    /// Convert it to gray
    cvtColor(img, src_gray, COLOR_BGR2BGRA);

    /// Generate grad_x and grad_y
    Mat grad_x, grad_y;
    Mat abs_grad_x, abs_grad_y;

    /// Gradient X
    //Scharr( src_gray, grad_x, ddepth, 1, 0, scale, delta, BORDER_DEFAULT );
    Sobel(src_gray, grad_x, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT);
    convertScaleAbs(grad_x, abs_grad_x);

    /// Gradient Y
    //Scharr( src_gray, grad_y, ddepth, 0, 1, scale, delta, BORDER_DEFAULT );
    Sobel(src_gray, grad_y, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT);
    convertScaleAbs(grad_y, abs_grad_y);

    /// Total Gradient (approximate)
    addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad);

    imshow("Sobel", grad);

    waitKey(0);

    return grad;
}
