#pragma once

#include <opencv2/core/core.hpp>//Opencv core
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/ml.hpp>       //Opencv Machine Learning

#include "Console.h"
#include "CrossVal.h"

#define MAX_SVM 1
#define NUM_CATEGORIAS 2

class Classifier
{
public:
    Classifier();
    Classifier(bool isConfMatrix, bool isFolds, int folds);
    ~Classifier();

    int Classify(uchar type);

    int CrossTrain(int fold, uchar type, std::vector<std::vector<cv::String>>& treino);
    int CrossValid(int fold, uchar type, std::vector<cv::String>& valida             , std::vector<int>& conf);

    //SetMethods
    void SetSVMFileName(const char* m_name);
    void SetConfMatrix(bool active = false);
    
    void SetConsole (CConsole* console) { m_console  = console; }
   // void SetCrossVal(bool isFolds     , CCrossValidation* ccv) { m_isCrossValid = isFolds;  m_CrossVal = ccv;     }
    void SetFolds   (int folds        ) { m_folds    = folds;   }


    enum Classification
    {
        kCla_Init = 0,
        kCla_ORB = kCla_Init,
        kCla_SURF ,
        kCla_DENSE,
        kCla_SIFT ,
        kCla_Fim,
    };

    cv::Ptr<cv::ml::SVM>& Train(uchar type);

    cv::Ptr<cv::ml::SVM>& Load (uchar type, cv::BOWImgDescriptorExtractor &bowDE, int fold);
    cv::Ptr<cv::ml::SVM>& Load (uchar type, cv::BOWImgDescriptorExtractor &bowDE);

    cv::Mat Subtract(cv::Mat img1, cv::Mat img2);
    cv::Mat Subtract(const char * img_1, const char * img_2);

    cv::Mat CannyThreshold(int minthreshold,cv::Mat& img);
    cv::Mat SobelEdgeDetection(cv::Mat& img);

    void SiftCompare(cv::Mat img_object, cv::Mat img_scene);
    void OrbComapre (cv::Mat img_object, cv::Mat img_scene);
    void SurfCompare(cv::Mat img_object, cv::Mat img_scene);

private:
    
    CConsole*  m_console;

    std::vector < cv::Ptr<cv::ml::SVM> > m_vecSVMs;
    std::vector <std::string> m_vecImagesTreino;
    std::vector <std::string> m_vecImagesTeste;

    std::vector <cv::KeyPoint> Dense_kp(cv::Mat image);
    
    bool niceHomography(cv::Mat H);

    int m_NrSVM;
    CString m_SvmName;

    bool m_isConfMatix;
    bool m_isCrossValid;

    void LoadImages();
    void LoadTesteImages();

    void confusion_matrix(int fold,std::vector<int>& pred_labels, std::vector<int>& true_labels);

    int NormalClassify(uchar type);

    int m_folds;

    int const max_lowThreshold = 100;

};
