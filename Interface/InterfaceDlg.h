#pragma once

#include "classifier.h"
#include "CrossVal.h"
#include "Console.h"
#include "Settings.h"

// CInterfaceDlg dialog
class CInterfaceDlg : public CDialogEx
{
    // Construction
public:
    CInterfaceDlg(CWnd* pParent = nullptr);	// standard constructor
    ~CInterfaceDlg();
    // Dialog Data
#ifdef AFX_DESIGN_TIME
    enum { IDD = IDD_INTERFACE_DIALOG };
#endif

protected:
    virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


    enum { DETECTION = 0, CAPTURING = 1, CALIBRATED = 2 };

    // Implementation
protected:
    HICON m_hIcon;

    // Generated message map functions
    virtual BOOL OnInitDialog();
    afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
    afx_msg void OnPaint();
    afx_msg HCURSOR OnQueryDragIcon();


    DECLARE_MESSAGE_MAP()

public:

    /*Commandos de interface*/
    afx_msg void OnBtnTreinar();
    afx_msg void OnBtnClassificar();
    afx_msg void OnBtnCrossVal();
    afx_msg void OnCbnSelchangeCombo1();
    afx_msg void OnShowImg();
    afx_msg void OnBtnSub();
    afx_msg void OnBtn3D();
    afx_msg void OnBtnBrw1();
    afx_msg void OnBtnBrw2();
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnBtnCompare();
    afx_msg void OnBtnSubtractAlter();
    afx_msg void OnBtnCanny();
    afx_msg void OnCmbColor();
    afx_msg void OnBtnSobel();
    afx_msg void OnBtnMulti3D();
    afx_msg void OnBtn3dTestes();


private:
    void FillComboType();
    void FillComboColor();
    void FillImgList();

    uchar m_type;
    cv::ImreadModes m_color;
    CString m_SvmName;

    CButton       m_btnTreinar;
    CButton       m_btnClassificar;
    CButton       m_btnCrossValida;
    CButton       m_btn3d;
    CButton       m_btn_sub;
    CButton       m_chk_ConfMatrix;
    CButton       m_chk_GreyFirst;
    CComboBox     m_cb_Choose;
    CComboBox     m_cmb_color;
    CCheckListBox m_lst_Img;
    CEdit         m_edt_svm_name;
    CEdit         m_edt_folds;
    CEdit         m_edt_img1;
    CEdit         m_edt_img2;
    CEdit         m_edt_Nrimages;

    CSliderCtrl   m_slider;
    CStatic       m_static_slider;

    Classifier       m_classifier;
    CCrossValidation m_crossVal;
    CConsole         m_console;
    Settings         m_settings;

    CString m_echo_Slider;

    cv::Mat m_subtraction_out;

};
