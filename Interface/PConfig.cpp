#include "stdafx.h"
#include "PConfig.h"
#include <string.h>

#define DEFAULT_SECTION "INSPFOCUS"
#define FIELD_RESULTS   "results"
#define FIELD_TRAIN     "training"
#define FIELD_DUMP1     "DUMP1"
#define FIELD_DUMP2     "DUMP2"
#define FIELD_CAMOUT    "CAMOUT"
#define FIELD_TESTE     "teste"
#define FIELD_SVM       "svm"
#define FIELD_CATEG     "categorias"

#define DEFAULT_RESULTS "C:\\Teste BOW SVM\\resultados\\"
#define DEFAULT_TRAIN  	"C:\\Teste BOW SVM\\treino\\"
#define DEFAULT_TESTE  	"C:\\Teste BOW SVM\\teste\\"
#define DEFAULT_SVM    	"C:\\Teste BOW SVM\\"
#define DEFAULT_DUMP1   "C:\\Teste BOW SVM\\Dump1\\"
#define DEFAULT_DUMP2   "C:\\Teste BOW SVM\\Dump2\\"
#define DEFAULT_CAMOUT  "C:\\Teste BOW SVM\\CamOut\\"

#define DEFAULT_FILEPATH "..\\x64\\Interface.ini"

using namespace std;


int GetNumCategorias()
{
    static char buff[10 + 1];
    GetPrivateProfileString(DEFAULT_SECTION, FIELD_CATEG, "2", buff, 10 + 1, nullptr);
    return StrToInt(buff);
}

const char* GetPathToResult()
{
    static char buff[256 + 1];

    GetPrivateProfileString(DEFAULT_SECTION, FIELD_RESULTS, DEFAULT_RESULTS, buff, 256 + 1, DEFAULT_FILEPATH);
    return buff;
}

const char* GetPathToTrain()
{
    static char buff[256 + 1]="";

    GetPrivateProfileString(DEFAULT_SECTION, FIELD_TRAIN, DEFAULT_TRAIN, buff, 256 + 1, DEFAULT_FILEPATH);
    return buff;
}

const char* GetPathToTest()
{
    static char buff[256 + 1];

    GetPrivateProfileString(DEFAULT_SECTION, FIELD_TESTE, DEFAULT_TESTE, buff, 256 + 1, DEFAULT_FILEPATH);
    return buff;
}

const char* GetPathToSVM()
{
    static  char buff[256 + 1];

    GetPrivateProfileString(DEFAULT_SECTION, FIELD_SVM, DEFAULT_SVM, buff, 256 + 1, DEFAULT_FILEPATH);

    return buff;
}

const char* GetPathToDump1()
{
    static  char buff[256 + 1];
    GetPrivateProfileString(DEFAULT_SECTION, FIELD_DUMP1, DEFAULT_DUMP1, buff, 256 + 1, DEFAULT_FILEPATH);
    return buff;
}

const char* GetPathToDump2()
{
    static  char buff[256 + 1];
    GetPrivateProfileString(DEFAULT_SECTION, FIELD_DUMP2, DEFAULT_DUMP2, buff, 256 + 1, DEFAULT_FILEPATH);
    return buff;
}

const char* GetPathToCameraOutput()
{
    static  char buff[256 + 1];
    GetPrivateProfileString(DEFAULT_SECTION, FIELD_CAMOUT, DEFAULT_CAMOUT, buff, 256 + 1, DEFAULT_FILEPATH);
    return buff;
}