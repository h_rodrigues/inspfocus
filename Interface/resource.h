//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Interface.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_INTERFACE_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDC_BTN_TREINAR                 1000
#define IDC_CMB_TYPE                    1001
#define IDC_LIST_IMG                    1004
#define IDC_BUTTON_CLASSIFICAR          1006
#define IDC_STATIC_PARA                 1008
#define IDC_EDT_SVM_NAME                1009
#define IDC_CHK_CONFMATRIX              1010
#define IDC_BT_CROSS                    1012
#define IDC_EDT_FOLDS                   1013
#define IDC_EDT_IMG1                    1014
#define IDC_EDT_IMG2                    1015
#define IDC_BTN_SUB                     1016
#define IDC_BT_3D                       1018
#define IDC_BTN_BRW1                    1019
#define IDC_BTN_BRW2                    1020
#define IDC_SLIDER1                     1021
#define IDC_STATIC_DES                  1022
#define IDC_BTN_CMP                     1023
#define IDC_BTN_ALTERN                  1024
#define IDC_CHCK_GREY                   1025
#define IDC_BTN_CANNY                   1026
#define IDC_CMB_CLR                     1027
#define IDC_BTN_SOBEL                   1028
#define IDC_BTN_MULTI3D                 1029
#define IDC_EDT_NRIMAGES                1030
#define IDC_BUTTON1                     1031
#define IDC_BTN_3DTESTES                1031

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1032
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
