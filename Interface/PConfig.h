#pragma once
#include <vector>
#include <string>

int GetNumCategorias();
const char* GetPathToResult();
const char* GetPathToTrain();
const char* GetPathToTest();
const char* GetPathToSVM();
const char* GetPathToCameraOutput();


const char* GetPathToDump1();
const char* GetPathToDump2();
