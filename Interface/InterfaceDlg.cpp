#include "stdafx.h"
#include "afxdialogex.h"

#include <string.h>
#include <algorithm>            // std::min, std::max
#include <fstream>              // File IO
#include <iostream>             // Terminal IO
#include <sstream>              // Stringstreams
#include <ctime>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>

#include <librealsense2/rs.hpp> // Include RealSense Cross Platform API
#include <librealsense2/rs_advanced_mode.hpp> //Advanced Mode

// 3rd party header for writing png files
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#include "example.hpp"          // Include short list of convenience functions for rendering
#include "Interface.h"
#include "InterfaceDlg.h"

#include "About.h"

void metadata_to_csv(const rs2::frame& frm, const std::string& filename);


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace std;
using namespace cv;


CInterfaceDlg::CInterfaceDlg(CWnd* pParent /*=nullptr*/)
    : CDialogEx(IDD_INTERFACE_DIALOG, pParent)
{
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
    m_type = Classifier::kCla_SIFT;
    m_color = IMREAD_GRAYSCALE;

    if (!m_console.Create("Debug Console", true))
    {
        MessageBox("Debug console failed to start", "Error", MB_ICONEXCLAMATION | MB_OK);
    }
    m_console.Show();

    m_crossVal  .SetConsole(&m_console);
    m_classifier.SetConsole(&m_console);
}

CInterfaceDlg::~CInterfaceDlg()
{
    m_console.Close();
}

void CInterfaceDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_CMB_TYPE          , m_cb_Choose );
    DDX_Control(pDX, IDC_BTN_TREINAR       , m_btnTreinar);
    DDX_Control(pDX, IDC_LIST_IMG          , m_lst_Img   );
    DDX_Control(pDX, IDC_BUTTON_CLASSIFICAR, m_btnClassificar);
    DDX_Control(pDX, IDC_EDT_SVM_NAME      , m_edt_svm_name);
    DDX_Control(pDX, IDC_CHK_CONFMATRIX    , m_chk_ConfMatrix);
    DDX_Control(pDX, IDC_BT_CROSS          , m_btnCrossValida);
    DDX_Control(pDX, IDC_EDT_FOLDS         , m_edt_folds);
    DDX_Control(pDX, IDC_EDT_IMG1          , m_edt_img1);
    DDX_Control(pDX, IDC_EDT_IMG2          , m_edt_img2);
    DDX_Control(pDX, IDC_BTN_SUB           , m_btn_sub );
    DDX_Control(pDX, IDC_BT_3D             , m_btn3d);
    DDX_Control(pDX, IDC_SLIDER1           , m_slider);
    DDX_Control(pDX, IDC_STATIC_DES        , m_static_slider);
    DDX_Control(pDX, IDC_CHCK_GREY         , m_chk_GreyFirst);
    DDX_Control(pDX, IDC_CMB_CLR           , m_cmb_color);    
    DDX_Control(pDX, IDC_EDT_NRIMAGES      , m_edt_Nrimages);
}                                            

BEGIN_MESSAGE_MAP(CInterfaceDlg, CDialogEx)
    ON_BN_CLICKED (IDC_BTN_TREINAR       , OnBtnTreinar)
    ON_BN_CLICKED (IDC_BUTTON_CLASSIFICAR, OnBtnClassificar)
    ON_BN_CLICKED (IDC_BT_CROSS          , OnBtnCrossVal)
    ON_BN_CLICKED (IDC_BTN_SUB           , OnBtnSub)
    ON_BN_CLICKED (IDC_BT_3D             , OnBtn3D )
    ON_BN_CLICKED (IDC_BTN_BRW1          , OnBtnBrw1 )
    ON_BN_CLICKED (IDC_BTN_BRW2          , OnBtnBrw2 )
    ON_BN_CLICKED (IDC_BTN_CMP           , OnBtnCompare)
    ON_BN_CLICKED (IDC_BTN_ALTERN        , OnBtnSubtractAlter)
    ON_BN_CLICKED (IDC_BTN_CANNY         , OnBtnCanny)
    ON_BN_CLICKED (IDC_BTN_SOBEL         , OnBtnSobel)
    ON_BN_CLICKED (IDC_BTN_MULTI3D       , OnBtnMulti3D)
    ON_BN_CLICKED (IDC_BTN_3DTESTES      , OnBtn3dTestes)

    ON_CBN_SELCHANGE(IDC_CMB_TYPE   , OnCbnSelchangeCombo1)
    ON_CBN_SELCHANGE(IDC_CMB_CLR    , OnCmbColor)
    
    ON_LBN_DBLCLK   (IDC_LIST_IMG   , OnShowImg)

    ON_WM_HSCROLL()
    
    ON_WM_SYSCOMMAND()
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
END_MESSAGE_MAP()

BOOL CInterfaceDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();
    // Add "About..." menu item to system menu.

    // IDM_ABOUTBOX must be in the system command range.
    ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
    ASSERT(IDM_ABOUTBOX < 0xF000);

    CMenu* pSysMenu = GetSystemMenu(FALSE);
    if (pSysMenu != nullptr)
    {
        BOOL bNameValid;
        CString strAboutMenu;
        bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
        ASSERT(bNameValid);
        if (!strAboutMenu.IsEmpty())
        {
            pSysMenu->AppendMenu(MF_SEPARATOR);
            pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
        }
    }

    // Set the icon for this dialog.  The framework does this automatically
    //  when the application's main window is not a dialog
    SetIcon(m_hIcon, TRUE);			// Set big icon
    SetIcon(m_hIcon, FALSE);		// Set small icon

    // TODO: Add extra initialization here
    FillComboType();
    FillComboColor();

    FillImgList();

    m_slider.SetRangeMin(0);
    m_slider.SetRangeMax(100);
    m_slider.SetPos(0);

    m_echo_Slider.Format(_T("%d"), 0);

    m_edt_folds.SetWindowText("5");

    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CInterfaceDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
    if ((nID & 0xFFF0) == IDM_ABOUTBOX)
    {
        CAboutDlg dlgAbout;
        dlgAbout.DoModal();
    }
    else
    {
        CDialogEx::OnSysCommand(nID, lParam);
    }
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CInterfaceDlg::OnPaint()
{
    if (IsIconic())
    {
        CPaintDC dc(this); // device context for painting

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // Center icon in client rectangle
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // Draw the icon
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialogEx::OnPaint();
    }
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CInterfaceDlg::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}

void CInterfaceDlg::OnBtnTreinar()
{
    m_edt_svm_name.GetLine(0,m_SvmName.GetBuffer());
    m_classifier.SetSVMFileName(m_SvmName.GetString());
    m_classifier.Train(m_type);
}

void CInterfaceDlg::OnBtnClassificar()
{
    vector<CString> aux;
    vector<int>     res;

    m_classifier.SetConfMatrix(m_chk_ConfMatrix.GetCheck());
}

void CInterfaceDlg::OnBtnCrossVal()
{
    CString szAux;
    m_edt_svm_name.GetLine(0, m_SvmName.GetBuffer());
    m_classifier.SetSVMFileName(m_SvmName.GetString());
    m_classifier.SetConfMatrix(m_chk_ConfMatrix.GetCheck());

    m_edt_folds.GetLine(0, szAux.GetBuffer());

    int n_Folds = atoi(szAux.GetBuffer());

    m_crossVal.SetFolds(n_Folds);
    m_crossVal.FillVectors();
    for (int i = 0; i < n_Folds; i++)
    {
        m_crossVal.KFold(i);
        cout << "Training fold :" << i << endl;
        m_classifier.CrossTrain(i, m_type, m_crossVal.GetTrainingData());
    }

    for (int i = 0; i < n_Folds; i++)
    {
        m_crossVal.KFold(i);
        cout << "Validation fold :" << i << endl;
        m_classifier.CrossValid(i, m_type, m_crossVal.GetValidationData(), m_crossVal.GetConfisionMatrix());
    }
}


void CInterfaceDlg::OnCbnSelchangeCombo1()
{
    m_type = static_cast<uchar>(m_cb_Choose.GetItemData(m_cb_Choose.GetCurSel()));
}

void CInterfaceDlg::OnShowImg()
{
    CString filepath;
    m_lst_Img.GetText(m_lst_Img.GetCurSel(),filepath);
    Mat img = imread(filepath.GetString(), cv::IMREAD_COLOR);
    imshow(filepath.GetString(), img);
}

void CInterfaceDlg::FillComboColor()
{
    for (uchar i = Classifier::kCla_Init; i < Classifier::kCla_Fim; i++)
    {
        m_cb_Choose.SetItemHeight(i, 15);
        switch (i)
        {
            case Classifier::kCla_DENSE : 
            {
                m_cb_Choose.InsertString(i, "Dense");
                m_cb_Choose.SetItemData(i, Classifier::kCla_DENSE);
            }break;
            case Classifier::kCla_ORB:
            {
                m_cb_Choose.InsertString(i, "ORB");
                m_cb_Choose.SetItemData(i, Classifier::kCla_ORB);
            }break;
            case Classifier::kCla_SURF:
            {
                m_cb_Choose.InsertString(i, "SURF");
                m_cb_Choose.SetItemData(i, Classifier::kCla_SURF);
            }break;
            case Classifier::kCla_SIFT:
            {
                m_cb_Choose.InsertString(i, "SIFT");
                m_cb_Choose.SetItemData(i, Classifier::kCla_SIFT);
            }break;
            default:break;
        }
    }
    m_cb_Choose.SetCurSel(Classifier::kCla_SIFT);
}


void CInterfaceDlg::FillComboType()
{
    m_cmb_color.InsertString(0, "Un changed");
    m_cmb_color.SetItemData (0, IMREAD_UNCHANGED);
    m_cmb_color.InsertString(1, "Gray Scale");
    m_cmb_color.SetItemData (1, IMREAD_GRAYSCALE);
    m_cmb_color.InsertString(2, "Color");
    m_cmb_color.SetItemData (2, IMREAD_COLOR);
    m_cmb_color.InsertString(3, "Any Depth");
    m_cmb_color.SetItemData (3, IMREAD_ANYDEPTH);
    m_cmb_color.InsertString(4, "Any Color");
    m_cmb_color.SetItemData (4, IMREAD_ANYCOLOR);
    m_cmb_color.InsertString(5, "Reduced GreyScale");
    m_cmb_color.SetItemData (5, IMREAD_REDUCED_GRAYSCALE_2);
    m_cmb_color.InsertString(6, "Load GDal");
    m_cmb_color.SetItemData (6, IMREAD_LOAD_GDAL);
    m_cmb_color.InsertString(7, "Reduced Color");
    m_cmb_color.SetItemData (7, IMREAD_REDUCED_COLOR_2);
    m_cmb_color.InsertString(8, "Reduced GreyScale 1//4");
    m_cmb_color.SetItemData (8, IMREAD_REDUCED_GRAYSCALE_4);
    m_cmb_color.InsertString(9, "Reduced Color 1//4");
    m_cmb_color.SetItemData (9, IMREAD_REDUCED_COLOR_4);
    m_cmb_color.InsertString(11, "Reduced GreyScale 1//8");
    m_cmb_color.SetItemData (11, IMREAD_REDUCED_GRAYSCALE_8);
    m_cmb_color.InsertString(12, "Reduced Color 1//8");
    m_cmb_color.SetItemData (12, IMREAD_REDUCED_COLOR_8);
    m_cmb_color.InsertString(13, "Ignore Orientation");
    m_cmb_color.SetItemData (13, IMREAD_IGNORE_ORIENTATION);
    
    m_cmb_color.SetCurSel(1);
}


void CInterfaceDlg::FillImgList()
{
    vector<String> auxLista;
    String auxConv;

    auxConv = GetPathToDump1();
    glob(auxConv, auxLista, false);

    for (size_t j = 0; j < auxLista.size(); j++)
    {
        int index = m_lst_Img.AddString(auxLista[j].c_str());
    }
}

void CInterfaceDlg::OnBtnSub()
{
    CString img1 , img2;
    string  img_1, img_2;
    m_edt_img1.GetWindowText(img1);
    m_edt_img2.GetWindowText(img2);

    m_subtraction_out = m_classifier.Subtract(img1.GetString(), img2.GetString());

 }

// Helper functions
void register_glfw_callbacks(window& app, glfw_state& app_state);

void CInterfaceDlg::OnBtn3D()
{

    // Declare RealSense pipeline, encapsulating the actual device and sensors
    rs2::pipeline pipe;

    // We want the points object to be persistent so we can display the last cloud when a frame drops
    rs2::points points;

    rs2::config config;

    config.enable_stream(rs2_stream::RS2_STREAM_COLOR   , 640, 480, rs2_format::RS2_FORMAT_RGBA8, 30);
    config.enable_stream(rs2_stream::RS2_STREAM_DEPTH   );
    config.enable_stream(rs2_stream::RS2_STREAM_INFRARED);

    rs2::spatial_filter      filter_0;
    rs2::temporal_filter     filter_1;
    rs2::hole_filling_filter filter_2;
    rs2::decimation_filter   filter_3;

    // Declare pointcloud object, for calculating pointclouds and texture mappings
    rs2::pointcloud pc;

    try
    {
        // Create a simple OpenGL window for rendering:
        window app(640, 480, "RealSense Pointcloud Example");
        // Construct an object to manage view state
        glfw_state app_state;
        // register callbacks to allow manipulation of the pointcloud
        register_glfw_callbacks(app, app_state);

        auto profile = pipe.start(config);

        while (app) // Application still alive?
        {
            // Wait for the next set of frames from the camera
            auto frames = pipe.wait_for_frames();

            auto depth  = frames.get_depth_frame();

            auto filtered_frame_0 = filter_0.process(depth);
            auto filtered_frame_1 = filter_2.process(filtered_frame_0);
            auto filtered_frame_2 = filter_1.process(filtered_frame_1);
            auto filtered_frame_3 = filter_3.process(filtered_frame_2);


            // Generate the pointcloud and texture mappings
            points = pc.calculate(filtered_frame_3);

            auto color= frames.get_color_frame();
            
            // For cameras that don't have RGB sensor, we'll map the pointcloud to infrared instead of color
            if (!color)
                color = frames.get_infrared_frame();

            // Tell pointcloud object to map to this color frame
            pc.map_to(color);

            // Upload the color frame to OpenGL
            app_state.tex.upload(color);

            // Draw the pointcloud
            draw_pointcloud(app.width(), app.height(), app_state, points);
        }

    }
    catch (const rs2::error & e)
    {
        std::cout << "RealSense error calling " << e.get_failed_function() << "(" << e.get_failed_args() << "):\n    " << e.what() << std::endl;
        return ;
    }
    catch (const std::exception & e)
    {
        std::cout << e.what() << std::endl;
        return;
    }

   // try
   // {
   //    
   //     // Declare depth colorizer for pretty visualization of depth data
   //     rs2::colorizer color_map;
   //
   //     // Declare RealSense pipeline, encapsulating the actual device and sensors
   //     rs2::pipeline pipe;
   //
   //     // Start streaming with default recommended configuration
   //     pipe.start(config);
   //
   //     // Capture 30 frames to give autoexposure, etc. a chance to settle
   //     for (auto i = 0; i < 100; ++i) pipe.wait_for_frames();
   //
   //     // Wait for the next set of frames from the camera. Now that autoexposure, etc.
   //     // has settled, we will write these to disk
   //
   //     std::time_t result = std::time(nullptr);
   //
   //     for (auto&& frame : pipe.wait_for_frames())
   //     {
   //         //if (auto df = frame.as<rs2::depth_frame>())
   //         //{
   //         //    auto filtered_frame_0 = filter_0.process(df);
   //         //    auto filtered_frame_1 = filter_1.process(filtered_frame_0);
   //         //    auto filtered_frame_2 = filter_2.process(filtered_frame_1);
   //         //    auto filtered_frame_3 = filter_3.process(filtered_frame_2);
   //         //
   //         //    if (auto vf = filtered_frame_3.as<rs2::video_frame>())
   //         //    {
   //         //        // Write images to disk
   //         //        std::stringstream png_file;
   //         //
   //         //        vf = color_map(filtered_frame_3);
   //         //
   //         //        std::time_t result = std::time(nullptr);
   //         //
   //         //        png_file << "New_" << vf.get_profile().stream_name() << "_" << result << ".png";
   //         //        stbi_write_png(png_file.str().c_str(), vf.get_width(), vf.get_height(),
   //         //            vf.get_bytes_per_pixel(), vf.get_data(), vf.get_stride_in_bytes());
   //         //        std::cout << "Saved " << png_file.str() << std::endl;
   //         //    }
   //         //
   //         //}
   //         // We can only save video frames as pngs, so we skip the rest
   //         if (auto vf = frame.as<rs2::video_frame>())
   //         {
   //             //auto stream = frame.get_profile().stream_type();
   //             // Use the colorizer to get an rgb image for the depth stream
   //             if (vf.is<rs2::depth_frame>())
   //             {
   //                 auto df = frame.as<rs2::depth_frame>();
   //
   //                 auto filtered_frame_0 = filter_0.process(df);
   //                 auto filtered_frame_1 = filter_1.process(filtered_frame_0);
   //                 auto filtered_frame_2 = filter_2.process(filtered_frame_1);
   //                 auto filtered_frame_3 = filter_3.process(filtered_frame_2);
   //
   //                 vf = color_map(filtered_frame_3);
   //             }
   //             // Write images to disk
   //             std::stringstream png_file;
   //
   //             png_file << "New-" << vf.get_profile().stream_name() << result <<".png";
   //             stbi_write_png(png_file.str().c_str(), vf.get_width(), vf.get_height(),
   //                 vf.get_bytes_per_pixel(), vf.get_data(), vf.get_stride_in_bytes());
   //             std::cout << "Saved " << png_file.str() << std::endl;
   //
   //             // Record per-frame metadata for UVC streams
   //             std::stringstream csv_file;
   //             csv_file << "rs-save-to-disk-output-" << vf.get_profile().stream_name() << result << "-metadata.csv";
   //             metadata_to_csv(vf, csv_file.str());
   //         }
   //     }
   // }
   // catch (const rs2::error & e)
   // {
   //     std::cerr << "RealSense error calling " << e.get_failed_function() << "(" << e.get_failed_args() << "):\n    " << e.what/() /<< std::endl;
   // }
   // catch (const std::exception & e)
   // {
   //     std::cerr << e.what() << std::endl;
   // }
   //
   //
}

void CInterfaceDlg::OnBtnBrw1()
{
    IFileOpenDialog *pfd;
    CString sz;

    // CoCreate the dialog object.
    HRESULT hr = CoCreateInstance(CLSID_FileOpenDialog,
        NULL,
        CLSCTX_INPROC_SERVER,
        IID_PPV_ARGS(&pfd));

    if (SUCCEEDED(hr))
    {
        DWORD dwOptions;
        // Specify multiselect.
        hr = pfd->GetOptions(&dwOptions);

        if (SUCCEEDED(hr))
        {
            hr = pfd->SetOptions(dwOptions);
        }

        if (SUCCEEDED(hr))
        {
            // Show the Open dialog.
            hr = pfd->Show(NULL);

            if (SUCCEEDED(hr))
            {
                // Obtain the result of the user interaction.
                IShellItem *psiResult;
                hr = pfd->GetResult(&psiResult);

                if (SUCCEEDED(hr))
                {
                    PWSTR pszFilePath = NULL;
                    psiResult->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);
                    
                    sz = pszFilePath;
                    m_edt_img1.SetWindowText(sz.GetString());

                    psiResult->Release();
                }
            }
        }
        pfd->Release();
    }
}

void CInterfaceDlg::OnBtnBrw2()
{
    IFileOpenDialog *pfd;
    CString sz;

    // CoCreate the dialog object.
    HRESULT hr = CoCreateInstance(CLSID_FileOpenDialog,
        NULL,
        CLSCTX_INPROC_SERVER,
        IID_PPV_ARGS(&pfd));

    if (SUCCEEDED(hr))
    {
        DWORD dwOptions;
        // Specify multiselect.
        hr = pfd->GetOptions(&dwOptions);

        if (SUCCEEDED(hr))
        {
            hr = pfd->SetOptions(dwOptions);
        }

        if (SUCCEEDED(hr))
        {
            // Show the Open dialog.
            hr = pfd->Show(NULL);

            if (SUCCEEDED(hr))
            {
                // Obtain the result of the user interaction.
                IShellItem *psiResult;
                hr = pfd->GetResult(&psiResult);

                if (SUCCEEDED(hr))
                {
                    PWSTR pszFilePath = NULL;
                    psiResult->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);

                    sz = pszFilePath;
                    m_edt_img2.SetWindowText(sz.GetString());

                    psiResult->Release();
                }
            }
        }
        pfd->Release();
    }
}

void CInterfaceDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
    if (pScrollBar == (CScrollBar *)&m_slider)
    {
        if (!m_chk_GreyFirst.GetCheck())
        {
            int value = m_slider.GetPos();
            m_echo_Slider.Format(_T("%d"), value);
            m_static_slider.SetWindowText(m_echo_Slider);
            if (!m_subtraction_out.empty())
            {
                m_classifier.CannyThreshold(value, m_subtraction_out);
            }
        }
        else
        {
            int value = m_slider.GetPos();
            m_echo_Slider.Format(_T("%d"), value);
            m_static_slider.SetWindowText(m_echo_Slider);
        }
    }
    else {
        CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
    }
}

void CInterfaceDlg::OnBtnCompare()
{
    CString img1, img2;
    m_edt_img1.GetWindowText(img1);
    m_edt_img2.GetWindowText(img2);

    Mat img_cmp1, img_cmp2;

    img_cmp1 = imread(img1.GetString(), IMREAD_COLOR);
    img_cmp2 = imread(img2.GetString(), IMREAD_COLOR);

    switch (m_type)
    {
    case Classifier::kCla_ORB:
    {
        m_classifier.OrbComapre (img_cmp1, img_cmp2);
    }break;
    case Classifier::kCla_SURF:
    {
        m_classifier.SurfCompare(img_cmp1, img_cmp2);
    }break;
    case Classifier::kCla_SIFT:
    {
        m_classifier.SiftCompare(img_cmp1, img_cmp2);
    }break;
     default:
        cout << "No compare made" << endl;
        break;
    }
}

void CInterfaceDlg::OnBtnSubtractAlter()
{
    int value = m_slider.GetPos();
    Mat canny1, canny2, img_org1, img_org2;
    CString img1, img2;
    m_edt_img1.GetWindowText(img1);
    m_edt_img2.GetWindowText(img2);

    img_org1 = imread(img1.GetString(), IMREAD_GRAYSCALE);
    img_org2 = imread(img2.GetString(), IMREAD_GRAYSCALE);
    
    if (img1.IsEmpty())
    {
        cout << "Imagem 1 está vazia" << endl;
        return;
    }
    if (img2.IsEmpty())
    {
        cout << "Imagem 2 está vazia" << endl;
        return;
    }
    canny1 = m_classifier.CannyThreshold(value, img_org1);
    canny2 = m_classifier.CannyThreshold(value, img_org2);

    m_classifier.Subtract(canny1, canny2);
}

void CInterfaceDlg::OnBtnCanny()
{
    int value = m_slider.GetPos();

    Mat img_org1;

    CString img1, img2;
    m_edt_img1.GetWindowText(img1);
    m_edt_img2.GetWindowText(img2);

    img_org1 = imread(img1.GetString(), m_color);

    m_classifier.CannyThreshold(value, img_org1);
}

void CInterfaceDlg::OnCmbColor()
{
    m_color = static_cast<ImreadModes>(m_cmb_color.GetItemData(m_cmb_color.GetCurSel()));
}

void metadata_to_csv(const rs2::frame& frm, const std::string& filename)
{
    std::ofstream csv;

    csv.open(filename);

    //    std::cout << "Writing metadata to " << filename << endl;
    csv << "Stream," << rs2_stream_to_string(frm.get_profile().stream_type()) << "\nMetadata Attribute,Value\n";

    // Record all the available metadata attributes
    for (size_t i = 0; i < RS2_FRAME_METADATA_COUNT; i++)
    {
        if (frm.supports_frame_metadata((rs2_frame_metadata_value)i))
        {
            csv << rs2_frame_metadata_to_string((rs2_frame_metadata_value)i) << ","
                << frm.get_frame_metadata((rs2_frame_metadata_value)i) << "\n";
        }
    }

    csv.close();
}

void CInterfaceDlg::OnBtnSobel()
{
    Mat img;
     
    CString img1;
    m_edt_img1.GetWindowText(img1);

    img= imread(img1.GetString(), m_color);

    m_classifier.SobelEdgeDetection(img);
}

void CInterfaceDlg::OnBtnMulti3D()
{
    // Declare RealSense pipeline, encapsulating the actual device and sensors
    rs2::pipeline pipe;

    // Declare de configuration and active streams from the device;
    rs2::config config;
    config.enable_stream(rs2_stream::RS2_STREAM_COLOR      , 1280, 720, rs2_format::RS2_FORMAT_RGBA8, 30);
    config.enable_stream(rs2_stream::RS2_STREAM_DEPTH      , 1280, 720, rs2_format::RS2_FORMAT_Z16, 30);
    config.enable_stream(rs2_stream::RS2_STREAM_INFRARED, 1, 1280, 720, rs2_format::RS2_FORMAT_Y8, 30);
    config.enable_stream(rs2_stream::RS2_STREAM_INFRARED, 2, 1280, 720, rs2_format::RS2_FORMAT_Y8, 30);

    rs2::spatial_filter      filter_0;
    rs2::temporal_filter     filter_1;
    rs2::hole_filling_filter filter_2;
    rs2::decimation_filter   filter_3;

    // Declare depth colorizer for pretty visualization of depth data
    rs2::colorizer color_map;

    CString strnr, svm;
    m_edt_Nrimages.GetWindowText(strnr);
    if (strnr.IsEmpty())
    {
        cout << "por vafor ponha um valor no nr de imagens." << endl;
        return;
    }
    m_edt_svm_name.GetWindowText(svm);


    int nrimages = atoi(strnr.GetString());

    try
    {
        rs2::context ctx;
        auto devices = ctx.query_devices();
        size_t device_count = devices.size();
        if (!device_count)
        {
            cout << "No device detected. Is it plugged in?\n";
            return;;
        }

        // Get the first connected device
        auto dev = devices[0];

        // Given a device, we can query its sensors using:
        std::vector<rs2::sensor> sensors = dev.query_sensors();

        rs2::sensor depth_sensor = sensors[0];

        bool isEmitteron = depth_sensor.get_option(RS2_OPTION_EMITTER_ENABLED);

        //float brightness  = depth_sensor.get_option(RS2_OPTION_BRIGHTNESS);

        //FUNCIONA A BASE DE SOMA DE 30
        float laser_power = depth_sensor.get_option(RS2_OPTION_LASER_POWER);
        laser_power = 120;
        depth_sensor.set_option(RS2_OPTION_LASER_POWER, laser_power);


        // Start streaming with default recommended configuration
        pipe.start(config);
        // Capture 50 frames to give autoexposure, etc. a chance to settle
        for (auto i = 0; i < 50; ++i) pipe.wait_for_frames();

        for (int i = 0; i < nrimages; i++)
        {
            auto frames = pipe.wait_for_frames();

            auto color      = frames.get_color_frame   ( );
            auto infrared_1 = frames.get_infrared_frame(1);
            auto infrared_2 = frames.get_infrared_frame(2);
            auto depth      = frames.get_depth_frame   ( );
            auto filtered_frame_0 = filter_2.process(depth);
            auto filtered_frame_1 = filter_0.process(filtered_frame_0);
            auto filtered_frame_2 = filter_1.process(filtered_frame_1);
            auto filtered_frame_3 = filter_3.process(filtered_frame_2);
            auto depth_processed  = color_map(filtered_frame_3);

            //Matrizes onde se vão ler a informação infravermelhar
            Mat ir1(Size(1280, 720), CV_8UC1, (void*)infrared_1.get_data(), Mat::AUTO_STEP);
            Mat ir2(Size(1280, 720), CV_8UC1, (void*)infrared_2.get_data(), Mat::AUTO_STEP);

            equalizeHist(ir1, ir1);
            equalizeHist(ir2, ir2);

            applyColorMap(ir1, ir1, COLORMAP_JET);
            applyColorMap(ir2, ir2, COLORMAP_JET);


            // Write images to disk
            std::stringstream depth_file,infrared_file1,ir1_file, infrared_file2, ir2_file,color_file;
            std::stringstream depth_csv_file, infrared_csv_file1, infrared_csv_file2, color_csv_file;

            depth_file << GetPathToCameraOutput() << "\\"<< svm.GetString() << "-" << depth_processed.get_profile().stream_name() << "_" << i << ".png";
            stbi_write_png(depth_file.str().c_str()             ,
                           depth_processed.get_width()          , 
                           depth_processed.get_height()         , 
                           depth_processed.get_bytes_per_pixel(), 
                           depth_processed.get_data()           , 
                           depth_processed.get_stride_in_bytes());
            std::cout << "Saved " << depth_file.str() << std::endl;

            depth_csv_file << GetPathToCameraOutput() << "\\" << "rs-save-to-disk-output-" << depth_processed.get_profile().stream_name() << "_" << i << "-metadata.csv";
            metadata_to_csv(depth_processed, depth_csv_file.str());

            ///////////Infrared 1
            infrared_file1 << GetPathToCameraOutput() << "\\" << svm.GetString() << "-" << infrared_1.get_profile().stream_name() << "_" << i << ".png";
            stbi_write_png(infrared_file1.str().c_str()             ,
                           infrared_1.get_width()          , 
                           infrared_1.get_height()         , 
                           infrared_1.get_bytes_per_pixel(), 
                           infrared_1.get_data()           , 
                           infrared_1.get_stride_in_bytes());
            std::cout << "Saved " << infrared_file1.str() << std::endl;

            infrared_csv_file1 << GetPathToCameraOutput()<<"\\" << "rs-save-to-disk-output-" << infrared_1.get_profile().stream_name() << "_" << i << "-metadata.csv";
            metadata_to_csv(infrared_1, infrared_csv_file1.str());

            ir1_file << GetPathToCameraOutput() << "\\Infrared1_ColorMap_" << svm.GetString() << "-" << infrared_1.get_profile().stream_name() << "_" << i << ".png";
            imwrite(ir1_file.str().c_str(), ir1);
            std::cout << "Saved " << ir1_file.str() << std::endl;

            ///////////Infrared 2
            infrared_file2 << GetPathToCameraOutput() << "\\" << svm.GetString() << "-" << infrared_2.get_profile().stream_name() << "_" << i << ".png";
            stbi_write_png(infrared_file2.str().c_str(),
                infrared_2.get_width(),
                infrared_2.get_height(),
                infrared_2.get_bytes_per_pixel(),
                infrared_2.get_data(),
                infrared_2.get_stride_in_bytes());
            std::cout << "Saved " << infrared_file1.str() << std::endl;

            infrared_csv_file2 << GetPathToCameraOutput() << "rs-save-to-disk-output-" << infrared_2.get_profile().stream_name() << "_" << i << "-metadata.csv";
            metadata_to_csv(infrared_2, infrared_csv_file2.str());

            ir2_file << GetPathToCameraOutput() << "\\Infrared2_ColorMap_" << svm.GetString() << "-" << infrared_2.get_profile().stream_name() << "_" << i << ".png";
            imwrite(ir2_file.str().c_str(), ir2);
            std::cout << "Saved " << ir2_file.str() << std::endl;

            //////////Color
            color_file << GetPathToCameraOutput() << "\\" << svm.GetString() << "-" << color.get_profile().stream_name() << "_" << i << ".png";
            stbi_write_png(color_file.str().c_str(),
                color.get_width(),
                color.get_height(),
                color.get_bytes_per_pixel(),
                color.get_data(),
                color.get_stride_in_bytes());
            std::cout << "Saved " << color_file.str() << std::endl;

            color_csv_file << GetPathToCameraOutput()<< "\\" << "rs-save-to-disk-output-" << color.get_profile().stream_name() << "_" << i << "-metadata.csv";
            metadata_to_csv(color, color_csv_file.str());


            //for (auto&& frame : pipe.wait_for_frames())
            //{
            //
            //    if (auto vf = frame.as<rs2::video_frame>())
            //    {
            //        //auto stream = frame.get_profile().stream_type();
            //        // Use the colorizer to get an rgb image for the depth stream
            //        if (vf.is<rs2::depth_frame>())
            //        {
            //            auto df = frame.as<rs2::depth_frame>();
            //
            //            auto filtered_frame_0 = filter_0.process(df);
            //            auto filtered_frame_1 = filter_1.process(filtered_frame_0);
            //            auto filtered_frame_2 = filter_2.process(filtered_frame_1);
            //            auto filtered_frame_3 = filter_3.process(filtered_frame_2);
            //
            //            vf = color_map(filtered_frame_3);
            //        }
            //        // Write images to disk
            //        std::stringstream png_file;
            //
            //        png_file << GetPathToCameraOutput() << "\\" << "New-" << svm.GetString() << "-" << vf.get_profile().stream_name() << "_" << i << ".png";
            //        stbi_write_png(png_file.str().c_str(), vf.get_width(), vf.get_height(), vf.get_bytes_per_pixel(), vf.get_data(), vf.get_stride_in_bytes());
            //        std::cout << "Saved " << png_file.str() << std::endl;
            //
            //        // Record per-frame metadata for UVC streams
            //        std::stringstream csv_file;
            //        csv_file << GetPathToCameraOutput() << "rs-save-to-disk-output-" << vf.get_profile().stream_name() << "_" << i << "-metadata.csv";
            //        metadata_to_csv(vf, csv_file.str());
            //    }
            //}
        }

        pipe.stop();
    }
    catch (const rs2::error & e)
    {
        std::cout << "RealSense error calling " << e.get_failed_function() << "(" << e.get_failed_args() << "):\n    " << e.what() << std::endl;
    }
    catch (const std::exception & e)
    {
        std::cout << e.what() << std::endl;
    }
}

void CInterfaceDlg::OnBtn3dTestes()
{
    CString svm;
    m_edt_svm_name.GetWindowText(svm);

    Ptr<StereoBM> bm = StereoBM::create(64, 37);
    Ptr<StereoSGBM> stereo = StereoSGBM::create(0,64,11);

    int SADWindowSize = 0;

    try
    {
        rs2::context ctx;
        auto devices = ctx.query_devices();
        size_t device_count = devices.size();
        if (!device_count)
        {
            cout << "No device detected. Is it plugged in?\n";
            return;;
        }

        // Get the first connected device
        auto dev = devices[0];

        // Given a device, we can query its sensors using:
        std::vector<rs2::sensor> sensors = dev.query_sensors();

        rs2::sensor depth_sensor = sensors[0];

        bool isEmitteron = depth_sensor.get_option(RS2_OPTION_EMITTER_ENABLED);

        //float brightness  = depth_sensor.get_option(RS2_OPTION_BRIGHTNESS);

        //FUNCIONA A BASE DE SOMA DE 30
        float laser_power = depth_sensor.get_option(RS2_OPTION_LASER_POWER);
        laser_power = 360;
        depth_sensor.set_option(RS2_OPTION_LASER_POWER, laser_power);

        // Check if current device supports advanced mode
        //if (dev.is<rs400::advanced_mode>())
        //{
        //    // Get the advanced mode functionality
        //    auto advanced_mode_dev = dev.as<rs400::advanced_mode>();
        //    const int max_val_mode = 2; // 0 - Get maximum values
        //                                // Get the maximum values of depth controls
        //    auto depth_control_group = advanced_mode_dev.get_depth_control(max_val_mode);
        //
        //    // Apply the depth controls maximum values
        //    advanced_mode_dev.set_depth_control(depth_control_group);
        //}

        // Declare RealSense pipeline, encapsulating the actual device and sensors
        rs2::pipeline pipe;
        // Declare depth colorizer for pretty visualization of depth data
        rs2::colorizer color_map;
        // Declare pointcloud object, for calculating pointclouds and texture mappings
        rs2::pointcloud pc;
        // We want the points object to be persistent so we can display the last cloud when a frame drops
        rs2::points points;


        rs2::spatial_filter      filter_0;
        rs2::temporal_filter     filter_1;
        rs2::hole_filling_filter filter_2;
        rs2::decimation_filter   filter_3;

        // Activate Streams
        rs2::config config;
        config.enable_stream(rs2_stream::RS2_STREAM_COLOR      , 640, 480, rs2_format::RS2_FORMAT_RGBA8, 30);
        config.enable_stream(rs2_stream::RS2_STREAM_DEPTH   ,    640, 480, rs2_format::RS2_FORMAT_Z16, 30);
        config.enable_stream(rs2_stream::RS2_STREAM_INFRARED, 1, 640, 480, rs2_format::RS2_FORMAT_Y8, 30);
        config.enable_stream(rs2_stream::RS2_STREAM_INFRARED, 2, 640, 480, rs2_format::RS2_FORMAT_Y8, 30);

       // window app(640, 480, "RealSense Pointcloud Example");
       // // Construct an object to manage view state
       // glfw_state app_state;
       // // register callbacks to allow manipulation of the pointcloud
       // register_glfw_callbacks(app, app_state);

        auto profile = pipe.start(config);

        for (auto i = 0; i < 50; ++i) pipe.wait_for_frames();

        //while (app) // Application still alive?
        //{
        //  auto frames = pipe.wait_for_frames();
        //
        //  auto depth = frames.get_depth_frame();
        //
        //  rs2::video_frame depthVideo = color_map(depth);
        // // auto color = frames.get_color_frame();
        //  auto infrared_1 = frames.get_infrared_frame(1);
        //  auto infrared_2 = frames.get_infrared_frame(2);
        //
        //  // Generate the pointcloud and texture mappings
        //  //points = pc.calculate(depth);
        //
        //  auto color = frames.get_color_frame();
        //
        //  //// For cameras that don't have RGB sensor, we'll map the pointcloud to infrared instead of color
        //  //if (!color)
        //  //    color = frames.get_infrared_frame();
        //  //
        //  //// Tell pointcloud object to map to this color frame
        //  //pc.map_to(color);
        //  //
        //  //// Upload the color frame to OpenGL
        //  //app_state.tex.upload(color);
        //
        //  // Draw the pointcloud
        ////  draw_pointcloud(app.width(), app.height(), app_state, points);
        //}

        auto frames = pipe.wait_for_frames();
        
        auto depth     = frames.get_depth_frame();

        auto filtered_frame_0 = filter_0.process(depth);
        auto filtered_frame_1 = filter_2.process(filtered_frame_0);
        auto filtered_frame_2 = filter_1.process(filtered_frame_1);
        auto filtered_frame_3 = filter_3.process(filtered_frame_2);

        rs2::video_frame depthVideo = color_map(filtered_frame_3);
        auto color     = frames.get_color_frame();
        auto infrared_1  = frames.get_infrared_frame(1);
        auto infrared_2  = frames.get_infrared_frame(2);

       // stereo = StereoSGBM::create(0,16,3);
        
        // Creating OpenCV matrix from IR image
        Mat ir1(Size(640, 480), CV_8UC1, (void*)infrared_1.get_data(), Mat::AUTO_STEP);
        Mat ir2(Size(640, 480), CV_8UC1, (void*)infrared_2.get_data(), Mat::AUTO_STEP);


        int numberOfDisparities = 0;
        numberOfDisparities = numberOfDisparities > 0 ? numberOfDisparities : ((ir2.size().width / 8) + 15) & -16;

        int sgbmWinSize = 0;
        sgbmWinSize = SADWindowSize > 0 ? SADWindowSize : 3;
        stereo->setBlockSize(sgbmWinSize);

        int cn = ir1.channels();
        stereo->setP1(8 * cn*sgbmWinSize*sgbmWinSize);
        stereo->setP2(32 * cn*sgbmWinSize*sgbmWinSize);
        stereo->setUniquenessRatio(10);
        stereo->setSpeckleWindowSize(150);
        stereo->setSpeckleRange(1);
        stereo->setDisp12MaxDiff(1);
        //stereo->setMode(StereoSGBM::MODE_HH);
        stereo->setMode(StereoSGBM::MODE_SGBM);
        //stereo->setMode(StereoSGBM::MODE_SGBM_3WAY);

        //bm->setNumDisparities(numberOfDisparities);

        Mat disparity;
        Mat disparity8(Size(640, 480), CV_8UC1);


        equalizeHist(ir1, ir1);
        equalizeHist(ir2, ir2);


        //bm->compute(ir1, ir2, disparity);
        stereo->compute(ir1, ir2, disparity);
        disparity.convertTo(disparity8, CV_8U, 255 / (64*16.));

        Mat depthMat(Size(640, 480), CV_8UC1, (void*)depthVideo.get_data(), Mat::AUTO_STEP);
        
        //cv::Mat depth8u = depthMat;
        //depth8u.convertTo(depth8u, CV_8UC1, 255.0 / 1000);
        
        // Apply Histogram Equalization
        applyColorMap(ir1, ir1, COLORMAP_JET);
        applyColorMap(ir2, ir2, COLORMAP_JET);
        
        // Apply Histogram Equalization
        //equalizeHist (depth8u, depth8u);
        //applyColorMap(depth8u, depth8u, COLORMAP_JET);
        
        // Display the image in GUI
        namedWindow("Display Image_1", WINDOW_AUTOSIZE);
        imshow("Display Image_1", ir1);
        namedWindow("Display Image_2", WINDOW_AUTOSIZE);
        imshow("Display Image_2", ir2);
        
       // namedWindow("Depth Image", WINDOW_AUTOSIZE);
       // imshow("Depth Image", depthMat);

        namedWindow("Disparity", WINDOW_AUTOSIZE);
        imshow("Disparity", disparity8);
        
        // Write images to disk
        std::stringstream png_file;
        
        png_file << GetPathToCameraOutput() << "\\" << "New-" << svm.GetString() << "-" << depthVideo.get_profile().stream_name() << ".png";
        stbi_write_png(png_file.str().c_str(), depthVideo.get_width(), depthVideo.get_height(), depthVideo.get_bytes_per_pixel(),depthVideo.get_data(), depthVideo.get_stride_in_bytes());
        std::cout << "Saved " << png_file.str() << std::endl;
        
        waitKey(0);
        
        pipe.stop();
        
    }
    catch (const rs2::error & e)
    {
        std::cout << "RealSense error calling " << e.get_failed_function() << "(" << e.get_failed_args() << "):\n    " << e.what() << std::endl;
        return;
    }
    catch (const std::exception & e)
    {
        std::cout << e.what() << std::endl;
        return;
    }

    
}
