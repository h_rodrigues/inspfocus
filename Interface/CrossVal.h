#pragma once
#include <vector>

#include "classifier.h"
#include <opencv2/core/core.hpp>//Opencv core


class CCrossValidation
{
public:
    CCrossValidation();
    ~CCrossValidation();

    void ResetOffset() {m_offset = 0; }
    void FillVectors();

    void SetFolds(int folds);
    void SetConsole(CConsole* console) { m_console = console; }

    std::vector<cv::String>&              GetValidationData() { return m_vec_validation; }
    std::vector<std::vector<cv::String>>& GetTrainingData()   { return m_vec_trainingdt; }
    std::vector<int>&                     GetConfisionMatrix(){ return m_vec_ConfMatrix; }

    void KFold(int fold);

protected:
    void Load();
    //Cria a separa��o do vetor em informa��o

private:
    int m_Folds;
    int m_offset;

    CConsole* m_console;


    //Vetor que vai ser prrenchido com as strings para todas as imagens
    std::vector<std::vector<cv::String>> m_vec_categories;

    //Vetor que vai ser alimentando para o svm
    std::vector<cv::String> m_vec_validation;
    std::vector<std::vector<cv::String>> m_vec_trainingdt;
    std::vector<int> m_vec_ConfMatrix;
};